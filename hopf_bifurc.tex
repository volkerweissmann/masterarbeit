\chapter{Supercritical Hopf bifurcations}\label{sec:hopf}
We now take a break from stochastic dynamics and look at deterministic systems that follow
\eq{\label{eq:2D_det}
    \partial_t\bv q(t) = \bv F(\bv q(t))\,.
}
The concept of a supercritical Hopf bifurcation is explained in this chapter.
We do this by using the Stuart-Landau oscillator, also called the cubic normal form of the Hopf bifurcation, as a prime example.
This oscillator is described in polar coordinates by \cite{Panteley}
\eq{\label{eq:stuart}
    F_r(r, \varphi) &= a r-b r^3
    \\
    F_\varphi(r, \varphi) &= c + d r^2 \,,
}
where $a,b,c,d$ are real coefficients. We restrict ourselves to the case $b>0$. Thus, while varying $a$, the system undergoes a supercritical Hopf bifurcation. This means that closed, periodic solutions in phase-space emerge as we will discuss in detail in the following.
\begin{figure}
    \includegraphics{plots/hopf_2D_neg.pdf}
    \caption{A trajectory of \eqref{eq:stuart} with $a=-1$, $b=1$, $c=d=5$. For $t\rightarrow\infty$, this converges to the only attractor: $x=0$, $y=0$. There is no limit cycle. In other words: for every starting point, $r$ decreases and approaches $0$.}
    \label{fig:hopf_2D_neg}
\end{figure}
\begin{figure}
    \includegraphics{plots/hopf_2D_pos.pdf}
    \caption{Two trajectories of \eqref{eq:stuart} with $a=1$, $b=1$, $c=d=5$. For $t\rightarrow\infty$, both converge to the only attractor: The limit cycle which is a circle with radius $r_0=\sqrt{a/b}=1$. In other words: If the initial $r$ is greater than $r_0$, it decreases and approaches $r_0$. If the initial $r$ is smaller than $r_0$, it increases and approaches $r_0$. In both cases $r\rightarrow r_0$ as $t\rightarrow\infty$.}
    \label{fig:hopf_2D_pos}
\end{figure}
For $a\leq 0$, any initial point $x(0)$ $y(0)$ will spiral inwards, towards the stable fix point $(0,0)^T$. An example trajectory is shown in figure \ref{fig:hopf_2D_neg}.
But for $a>0$ we see a qualitative different behavior, shown in figure \ref{fig:hopf_2D_pos}:
If $r(0)=r_0=\sqrt{a/b}$, it moves on a closed path, called the limit cycle, here a circle with radius $r_0$. If we start outside of the limit cycle, i.e. $r(0)>r_0$, we spiral inwards towards the limit cycle. If we start inside the limit cycle, i.e. $r(0)<r_0$, we spiral outwards towards the limit cycle.
In other words, no matter where we start\footnote{If we start at \emph{exactly} $x=0$, $y=0$, we stay there, no matter what $a$ is. But this has no relevant for this thesis, because $(0,0)^T$ is an unstable fixpoint for $a>0$ and we will look at noisy systems later.}, we end up doing the same cyclic motion, called the limit cycle.
This qualitative difference between $a\leq 0$ in $a>0$ is called a supercritical Hopf bifurcation.

For the Stuart-Landau oscillator, this behavior can be understood easily if we look at $F_r(r)=\partial_t r(t)$.
$F_r(r)$ is shown for $a=\pm 1$ in figure \ref{fig:hopf_1D}.
If $a<0$, then $F_r(r)<0$ for $r\in(0,\infty)$, therefore $r$ will decrease and approach $0$.
If $a>0$, then $F_r(r) >0$ for $r\in (0,r_0)$, therefore $r$ will increase and approach $r_0$ if it is initially below $r_0$. For $r\in(r_0,\infty)$, $F_r(r)<0$, therefore $r$ will decrease and approach $r_0$ if it is initially above $r_0$.

The limit cycle is stable, because the system will always relax to the limit cycle and stay in its proximity, even if we add some random perturbation to \eqref{eq:2D_det}. Adding noise to \eqref{eq:2D_det} results in Langevin dynamics. This will be done in the next chapter.

\begin{figure}
    \includegraphics{plots/hopf_1D.pdf}
    \caption{$F_r(r)=\partial_t r(t)$ from \eqref{eq:stuart} for $a=\pm 1$, $b=1$. We can see that time-evolving any point would end up at $r=0$ if $a=-1$ or at $r=r_0=\sqrt{a/b}=1$ if $a=+1$.}
    \label{fig:hopf_1D}
\end{figure}
