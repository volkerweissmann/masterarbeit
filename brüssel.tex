\chapter{The Brusselator}\label{sec:brussel}
The Brusselator is a paradigmatic example of a chemical oscillator \cite{basile}. Since it shows a noisy limit cycle it is at the intersection of the previous chapter and chapter \ref{sec:chem}. The Brusselator consists of a volume $\Omega$ containing two chemicals $X$ and $Y$. It is in contact with an external bath containing the chemicals $A$ and $B$ at fixed concentrations $[A]$ and $[B]$. The set of reactions is
\eq{\label{eq:chems}
    \mathrm{A} &\xrightleftharpoons[k_1^-]{k_1^+} \mathrm{X}\\
    \mathrm{B} &\xrightleftharpoons[k_2^-]{k_2^+} \mathrm{Y}\\
    2\mathrm{X}+\mathrm{Y} &\xrightleftharpoons[k_3^-]{k_3^+} 3\mathrm{X}\,,
}
where $k_i^\pm$ denotes the reaction rates. The state is given by $(X,Y)$.
% \url{https://doi.org/10.1063/1.432977}
% "A number of intriguing phenomena occur when inputs
% or boundary conditions which are incompatible with
% equilibrium are imposed on a macroscopic system. In
% the simplest situations a Single stable nonequilibrium
% steady state develops."\\
% "When external conditions
% are such that only unstable or both stable and unstable
% steady states occur, a system may undergo undamped
% oscillations as, for example, in the Belousov chemical
% reactions or Klihnes oscillating mercury drop."
Doing the calculations described in section \ref{sec:chem_to_lange} with \eqref{eq:chems}, we find that the Brusselator can be described by Langevin dynamics with
\eq{\label{eq:brussel_FQ}
    \bv F(x,y) &= \begin{pmatrix} k_1^+ [A] - k_1^- x + k_3^+ x^2 y - k_3^- x^3 \\
    k_2^+ [B] - k_2^- y - k_3^+ x^2 y + k_3^- x^3\end{pmatrix}\\
    \Qn(x,y) &= \begin{pmatrix}
        k_1^+ [A] + k_1^- x + k_3^+ x^2 y + k_3^- x^3 & -k_3^+ x^2 y - k_3^- x^3 \\
        -k_3^+ x^2 y - k_3^- x^3 & k_2^+ [B] + k_2^- y + k_3^+ x^2 y + k_3^- x^3
    \end{pmatrix}/2\\
    \epsilon &= 1/\Omega\,.
}
% The only restriction on the choice of $C$ is $C C^T\stackrel ! =2Q$\,. We choose
% \eq{
%     C &= \sqrt{2/Q_{11}}\begin{pmatrix}
%         Q_{11} & 0\\
%         Q_{12} & \sqrt{Q_{11}Q_{22} - Q_{12}^2}
%     \end{pmatrix}\,.
% }
Like Basile \cite{basile}, we choose our parameters to be
\eq{
    [A] &= 1 \\
    [B] &= 3 \\
    k_1^+ &= 0.1 \\
    k_1^- &= 1 \\
    k_2^+ &= 0.1 \\
    k_2^- &= e^{-\Delta\mu} \frac{k_1^- k_2^+ [B] k_3^+}{k_1^+ [A] k_3^-} \\
    k_3^+ &= 1\\
    k_3^- &= 1\,,
}
where $\Delta\mu$ is a parameter and the thermodynamic force associated with the cycle
\eq{
    (X,Y) \rightarrow (X,Y+1) \rightarrow (X+1,Y) \rightarrow (X,Y)\,.
}
The Brusselator undergoes a supercritical hopf bifurcation if $\Delta\mu$ is varied. Below the critical point $\Delta\mu_c\approx 3.93$ (see section \ref{sec:find_muc}) the system is qualitatively equivalent to the steward landau oscillator with $a< 0$. Above $\Delta\mu_c$, a limit cycle exists and the system is qualitatively equivalent to the steward landau oscillator with $a>0$. This is illustrated in figure \ref{fig:hopf_brüssel_neg} and \ref{fig:hopf_brüssel_pos}.

\subsection{Finding the bifurcation point $\Delta\mu_c$}\label{sec:find_muc}
Let $\bm q_0$ be the fixpoint of the brusselator, i.e.
\eq{
    \bm F(\bm q_0) &= 0\,.
}
We can approximate $\bm F$ around the fixpoint using the taylor polynomial
\eq{
    \bm F(\bm q) \approx \doubleunderline J \bm{\Delta q}
}
where $\doubleunderline J$ is the Jacobian matrix of $\bm F$ at $\bm q_0$ and $\bm{\Delta q}$ is defined as
\eq{
    \bm{\Delta q} &= \bm q - \bm q_0\,.
}
The deterministic dynamics $\partial_t\bm q(t) = \bm F(\bm q(t))$ imply the approximated dynamics
\eq{
    \partial_t \bm{\Delta q}(t) &= \doubleunderline J \bm{\Delta q}(t)\,,
}
with the solution
\eq{\label{eq:area_time_evolve}
    \bm{\Delta q}(t) &= e^{\doubleunderline J t} \bm{\Delta q}(0)\,.
}
The area of a parallelogram \cite{hefferon2020linear} is given by the determinant of the matrix formed by the vectors representing the parallelogram's sides.
Every area can be divided into parallelograms. Thus, if $\doubleunderline A$ is a matrix and $S$ is a surface with an area of $|S|$,
\eq{
    \{ \doubleunderline A \bv s : \bv s \in S \}
}
is a suface with an area of $|\det\doubleunderline A|\cdot|S|$. Since the determinant is the product of all eigenvalues and the trace is their sum, it follows
\eq{
    \det e^{\doubleunderline J t} &= e^{\tr \doubleunderline J t}\,.
}
The determinant on the left side is greater than one, if and only if $\tr\doubleunderline J > 0$.
The time-evolution \eqref{eq:area_time_evolve} is given by matrix-vector multiplication.
Thus, a surface in the phase space around the fixpoint will shrink if $\tr\doubleunderline J < 0$ and grow if $\tr\doubleunderline J > 0$. The fixpoint is stable in the former case and unstable in the latter case.
Numerically, we found the critical $\Delta\mu_c$ for a transition from $\tr\doubleunderline J < 0$ to $\tr\doubleunderline J > 0$ to be approximately $3.93$. This is a slight deviation from Basile's value of $\Delta\mu_c=3.95$ \cite{basile}.

\begin{figure}
    \includegraphics{plots/hopf_brüssel_neg.pdf}
    \caption{Example Brusselator trajectories with $\Delta\mu=3.8<\Delta\mu_c$ in the deterministic limit $\Omega\rightarrow\infty$. Similar to figure \ref{fig:hopf_2D_neg}, all trajectories approach the stable point.}
    \label{fig:hopf_brüssel_neg}
\end{figure}
\begin{figure}
    \includegraphics{plots/hopf_brüssel_pos.pdf}
    \caption{Example Brusselator trajectories with $\Delta\mu=4.4>\Delta\mu_c$ in the deterministic limit $\Omega\rightarrow\infty$. Similar to figure \ref{fig:hopf_2D_pos}, both trajectories approach the limit cycle.}
    \label{fig:hopf_brüssel_pos}
\end{figure}
