\chapter{Chemical Networks}\label{sec:chem}
In the last chapter we introduced the Langevin equation, the equivalent Fokker-Planck equation and the master equation. In section \ref{sec:chemmaster}, we present a paradigmatic example of a system that follows the master equation. In section \ref{sec:chem_to_lange}, we perform the limit of large system size. We find that large chemical networks follow Langevin dynamics. This is important for this work, because biochemical oscillators are chemical networks, connecting real-world examples to our theoretical work.

\section{Chemical master equation}\label{sec:chemmaster}
Our system of interest is a solution of $s$ different molecules. With the number of $X_i$ molecules denoted by $N_i$, the state of the system is given by $\{N_i\}$. We number the chemical reactions with $\rho=1,2,...,r$. The reaction $\rho$ can be written as
\eq{
    \sum_{i=1}^s \nu_i^{<\rho} X_i \xrightarrow[]{k_\rho} \sum_{i=1}^s \nu_i^{>\rho} X_i\,,
}
where the reaction rate $k_\rho$ is independent of $\{N_i\}$. $\nu_i^{<\rho}$ and $\nu_i^{>\rho}$ indicate how many molecules of species $i$ are consumed or created by this reaction. Thus, if the reaction $\rho$ occurs, the state transitions from  $\{N_i\}$ to $\{N_i+\nu_\rho^i\}$, where we have defined
\eq{
    \nu_i^\rho = \nu_i^{>\rho} - \nu_i^{<\rho}\,.
}
The rate of this transition is \cite{The_correlation_time_of_mesoscopic_chemical_clocks}
\eq{\label{eq:wrho}
    W_\rho(\{N_i\}) = \Omega k_\rho \prod_{i=1}^s \prod_{m=1}^{\nu_i^{<\rho}} \frac{N_i - m + 1}{\Omega}\,,
}
where $\Omega$ is the system size. This only works under the assumption that it is sufficient to just track the number of particles and not their position, velocity and state \cite{doi:10.1063/1.481811}.
We can intuitively explain this equation as follows:
\begin{itemize}
    \item The first $\Omega$ is there, because if the volume and the number of molecules are doubled, twice as many reactions will occur.
    %\item The $k_\rho$ is there to account for differences between different reactions.
    \item The $N_i-m+1$ is there because if the number of $X_i$ molecules increases, the probability of said $X_i$ molecule meeting the other required molecules increases.
    \item The $1/\Omega$ is there because if the volume increases, the probability of two molecules coming near each other decreases.
\end{itemize}
The dynamics are described by the chemical master equation \cite{The_correlation_time_of_mesoscopic_chemical_clocks}
\eq{\label{eq:wrhomaster}
    \frac d{dt} p(\{N_i\}, t) = \sum_{\rho=1}^r  W_\rho(\{N_i-\nu_i^\rho\}) p(\{N_i-\nu_i^\rho\}, t) - W_\rho(\{N_i\}) p(\{N_i\}, t)\,,
}
which is a special case of the master equation \eqref{eq:master}.

\section{Continuum limit leads to Langevin dynamics}\label{sec:chem_to_lange}
Instead of the number of molecules $N_i$, we could also track the number $n_\rho$ of $\rho$-reactions that occured. They follow the master equation
\eq{\label{eq:masterdelta}
    \frac d{dt} p(n_1, n_2, ... t) &= w_1(n_1-1,n_2,...) p(n_1-1, n_2, ... t) - w_1(n_1,n_2,...) p(n_1, n_2, ... t)
    \\&+ w_2(n_1,n_2-1,...) p(n_1, n_2-1, ... t) - w_2(n_1,n_2,...) p(n_1, n_2, ... t)
    \\&+ ...\,,
}
with
\eq{
    w_\rho(\{n_\rho\}) &= W_\rho\lou \left\{ N_i(0) + \sum_\rho n_\rho(t) \nu^\rho_i\right\} \rou
}
since
\eq{
    N_i(t) &= N_i(0) + \sum_\rho n_\rho(t) \nu^\rho_i\,.
}
For small $t$ and large $\Omega$, $n_\rho(t)/\Omega$ will be small, thus $w_\rho$ sill stay approximately constant. Then, \eqref{eq:masterdelta} will have the analytical solution
\eq{
    P(\{n_\rho\}, t) &= \prod_{\rho} \frac{(w_\rho t)^{n_\rho}}{n_\rho !}e^{-n_\rho t}\,.
}
In other words, $n_\rho$ is poisson distributed with a mean of $w_\rho t$.
This knowledge can be used to find a method of simulating trajectories:
At every timestep, we draw $r$ random variables $n_\rho$ that are poisson distributed with a mean of $W_\rho(t)\Delta t$ and then update $\{N_i\}$ according to
\eq{\label{eq:simu_poisson}
    N_i(t+\Delta t) = N_i(t) + \sum_\rho \nu^\rho_i n_\rho(t,t+\Delta t)\,.
}
If $\Delta t$ is chosen small and $\Omega$ is large, this generates trajectories with the same dynamics as the chemical master equation.
To get from this discrete dynamics to the continuous Langevin dynamics, we perform the continuum limit $\Omega\rightarrow\infty$, replacing the discrete $N_i$'s with continuous $q_i \equiv N_i/\Omega$.
Since $W_\rho\Delta t\rightarrow\infty$ for $\Omega\rightarrow\infty$ the Poisson distribution can be approximated by a Gaussian distribution with mean and variance $W_\rho\Delta t$, i.e.
\eq{
    p(n_\rho) &= \frac{1}{\sqrt{2\pi W_\rho\Delta t}}\exp\lou-\frac{(n_\rho-W_\rho\Delta t)^2}{2W_\rho\Delta t}\rou\,.
}
With this, \eqref{eq:simu_poisson} becomes
\eq{\label{eq:simple_maru}
    q_i(t+\Delta t) = q_i(t) + \sum_\rho \nu^\rho_i W_\rho/\Omega\Delta t + \sum_\rho \nu^\rho_i \sqrt{W_\rho\Delta t}/\Omega g_\rho(t)\,,
}
where each $g_\rho$ is Gaussian distributed with mean zero and
\eq{
    \lang g_\rho(t) g_{\rho'}(t')\rang &= \delta_{\rho,\rho'} \delta_{t,t'}\,.
}
This is the Euler-Maruyama method \cite{kloeden1992numerical} for solving the Langevin equation \eqref{eq:lange_ND} with
\eq{\label{eq:FC_large_chem}
    F_i(\{q_i\}) &= \sum_\rho \nu^\rho_i W_\rho(\{q_i\})\Delta t \\
    \hat C_{ij}(\{q_i\}) &= \nu_{i}^j \sqrt{W_j}\\
    \epsilon &= 1/\Omega\,.
}
To conclude, we have proven, like others before \cite{doi:10.1063/1.481811, Theoretical_study_of_robustness_factors}, that a chemical network follows Langevin dynamics in the continuum limit.
