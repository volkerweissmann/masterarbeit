\chapter{Numerical verification}\label{sec:numerics}
To verify the correctness of the previous chapters, we compared theoretical expectations to numerical results.

\section{Numerical scheme}\label{sec:scheme}
In this section, we describe a method to find the robustness of a system numerically. As in chapter \ref{sec:eigvals} we calculate
\eq{
    \lang f(\bv q(t))g(\bv q(0)) \rang\,,
}
where $f$ and $g$ are some $\mathbb R^n\rightarrow\mathbb R$ functions and the initial state $\bv q(0)$ is drawn from the distribution $p^0(\bv q)$. The ensemble average $\lang ...\rang$ is defined by averaging over infinitely many identical and independent systems. Because simulating infinitely many systems takes infinite computation time, we average over just $M\in\mathbb N$ systems. A lower $M$ leads to a shorter computation time but lower precision.

\subsection{Simulating a single trajectory}\label{sec:singletraj}
A single trajectory is simulated in a single thread.
We begin by drawing $\bv q(0)$ from our inital distribution $p^0(\bv q)$.
Every timestep, we draw some random values $\bv\zeta$ from a gaussian distribution fullfilling
\eq{
    \lang \bv \zeta(t) \rang &= 0 \\
    \lang \zeta_{i}(t) \zeta_{j}(t') \rang &= \delta_{i,j}\delta_{t,t'} \quad \forall i,j\,.
}
Then we update the position of our particle according to
\eq{
    \bv q_i(t+\Delta t) = \bv F_i(\bv q(t)) \Delta t + \Cn(\bv q(t)) \bv \zeta(t) \sqrt{\epsilon \Delta t } \,,
}
with $\Delta t$ chosen to be $10^{-3}$.
This is a time discretized version of the Langevin equation \eqref{eq:lange_ND}. Repeating this step is called the Euler-Maruyama method \cite{kloeden1992numerical}.
If $q_i$ is supposed to be in $[0,\per]$, we add or subtract $\per$ if necessary.
Every 10th timestep, we save the value of $f(\bv q(t))g(\bv q(0))$ into memory. If we did this after every timestep, the performance would become memory-bandwidth-bound.
Once $t$ reaches $T_s$, we return our $f(\bv q(t))g(\bv q(0))$ time series to the master thread.

\subsection{Calculating the ensemble average}
%If we want to average over $M$ trajectories, these $M$ jobs can be easily parallized.
The master thread holds a thread pool. If a thread becomes available we simulate a single trajectory as described in the previous section. Every time a single trajectory is finished, we add its $f(\bv q(t))g(\bv q(0))$ time series to a buffer that was zero-initialized and start the simulation of another trajectory. We can then free the buffer that holds this trajectory. If we would instead first simulate all trajectories and calculate the sum afterwards, memory would be exhausted. If all $M$ trajectories have been simulated and added to the buffer, we divide the buffer by $M$ to get the ensemble average
\eq{
    \lang f(\bv q(t))g(\bv q(0)) \rang
}
as a function of time within the interval $t\in[0,T_s]$.

\subsection{Analyzing the ensemble average}
We define
\eq{
    \cor(t) \defin \lang f(\bv q(t))g(\bv q(0)) \rang - \overline{\lang f(\bv q(\tau))g(\bv q(0)) \rang} \,,
}
where $\overline{h(\tau)}$ denotes the time average
\eq{
    \overline{h(\tau)} \equiv \frac 1 T_s \int_0^{T_s} \diff{\tau} h(\tau)\,.
}
$\cor(t)$ shows damped oscillations. We need to determine the period
\eq{
    T=\frac{2\pi}{\omega}
}
and the damping strength $\gamma$ of this oscillation, since the robustness is given by
\eq{
    R = \frac{\omega}{\gamma} = \frac{2\pi}{T\gamma}\,.
}
To do this, we first find the peaks. The period is then given by the average distance between the peaks. The damping strength can be determined by fitting
\eq{
    c e^{-\gamma t}
}
against the peaks, where both $c$ and $\gamma$ are fit parameters. We restrict the list of peaks used for this calculation to the ones inside $[t_1,t_2]$. Both $T$ and $\gamma$ will depend on $t_1$ and $t_2$. We are interested in the first nontrivial eigenvalue, i.e. the eigenfunction that decays the slowest. Thus we would ideally set $t_1$ and $t_2$ to something extremely large. But it takes more computing time to precisely calculate $\cor(t)$ for larger $t$. There are two reasons for this. First, larger $t$ require a larger $T_s$, i.e. more Euler-Maruyama steps. Second, if we have a fixed number of trajectories $M$, $\cor(t)$ is less precise for higher $t$. Therefore a higher $M$ is needed if we want $\cor(t)$ to be precise for larger $t$. We choose our values of $t_1$ and $t_2$ using a semi-scientific trial-and-error method. We sometimes choose different $t_1$ and $t_2$ for calculating the period and the damping strength.

\section{One-dimensional system}\label{sec:1Dnum}
As an example of a one-dimensional periodic system, we chose
\eq{
    F(q) &= 1 + a\sin(q) \\
    Q(q) &= 1 %\quad \leftrightarrow \quad  C(q) &= \sqrt{2}
    %\\\per &= 2\pi
    \,.
}
We kept $q$ in the interval $[0,2\pi]$, i.e $\per=2\pi$. Every trajectory started at $q=0$, i.e.
\eq{
    p^0(q) &= \delta(q)\,.
}
We chose
\eq{
    f(q) &= q \\
    g(q) &= 1\,,
}
resulting in
\eq{
    \cor(t) &= \lang q(t) \rang - \overline{\lang q(\tau) \rang}\,.
}
We averaged over $M=2^{20}$ trajectories for $\epsilon=10^{-4}$ and $M=2^{18}$ trajectories for $\epsilon=10^{-3},10^{-2}$. The length of those trajectories was $T_s=8000\pi$ for $\epsilon=10^{-4}$ and $T_s=800\pi$ for $\epsilon=10^{-3},10^{-2}$.
The robustness we calculated numerically is shown in figure \ref{fig:asin}, along with the theoretical predicitons of \eqref{eq:R_myint} and the upper bound \eqref{eq:fcont}.
There is good agreement between the theoretical values and the numerical values. For $a\approx\pm 1$ our code sometimes did not return any value for $R$, because for such a low $R$ the peaks decayed so fast that the second peak could not be found.
\eqref{eq:fcont} should be an upper bound on the robustness and this plot confirms this. For $\epsilon=10^{-2}$ and $a\approx 0$ the values are particular noisy and on average higher than the theory. Some datapoints in this region lie above the bound but they are likely numerical errors, because we  prove $R\epsilon=1$ for $a=0$, $\epsilon\in\mathbb R^+$ in section \ref{sec:const_cosexp}.
For $a=0$, force and diffusion are constant and equality with the upper bound holds, as shown in chapter \ref{sec:bound}.
As $|a|$ increases, the force gets less constant and the difference between the bound and $R$ increases.

\begin{figure}
    \includegraphics{plots/asin.pdf}
    \caption{Robustness of a $2\pi$-periodic one-dimensional Langevin system \\with $F(q)=1+a\sin(q)$, $Q(q)=1$. The ``Numerics'' curves were calculated as explained in section \ref{sec:1Dnum} and \ref{sec:scheme}. The ``Theory'' curve is given by \eqref{eq:R_myint}. The ``Upper boundary'' curve is given by \eqref{eq:fcont}.}
    \label{fig:asin}
\end{figure}

\section{Stuart-Landau oscillator}\label{sec:stuartnum}
The two-dimensional force is given by \eqref{eq:stuart} and the diffusivity is
\eq{
    \Qn(\bv q) &= \doubleunderline 1
    % \begin{pmatrix}
    %     1 & 0\\
    %     0 & 1
    % \end{pmatrix}
    % \quad \leftrightarrow \quad  \Cn = \sqrt{2}\begin{pmatrix}
    %     1 & 0\\
    %     0 & 1
    % \end{pmatrix}
    \,.
}
Every trajectory starts on the limit cycle at $\bv q=(r_0,0)^T$, i.e.
\eq{
    p^0(\bv q) &= \delta\lou \bv q-\begin{pmatrix}
        r_0\\0
    \end{pmatrix}\rou\,.
}
We chose
\eq{
    g(\bv q) &= 1
}
and $f(\bv q)$ to be the $x$ component of $\bv q$, resulting in
\eq{
    \cor(t) &= \lang x(t) \rang - \overline{\lang x(\tau) \rang}\,.
}
We averaged over $M=2^{14}$ trajectories. The length of those trajectories was $T_s=500$.
The numerically calculated robustness is shown in figure \ref{fig:stuart_numerics} alongside Gaspard's method from section \ref{sec:gaspard} and our theory \eqref{eq:R_myint}.
The numerical values agree with Gaspard's theory, except for $c\approx -d$. For $c=-d$, the angular component of the force vanishes and oscillations come to a halt, therefore $\omega$ and $R$ vanishes. Both our implementation of Gaspard's method and our numeric method have problems with $c\approx d$, although the problematic range is smaller for the numeric method than for Gaspard's method. For small $d$, the theory \eqref{eq:R_myint} matches with the numerics, but for larger $d$ the theory yields a higher robustness than the numerics. This is expected, since the phase-coupling is proportional to $d$, as explained in section \ref{sec:2Drobust}.

\begin{figure}
    \includegraphics{plots/stuart_numerics.pdf}
    \caption{Robustness of a Stuart-Landau oscillator. $\bv F(\bv q)$ is given by \eqref{eq:stuart} \\with $a=b=1$ and diffusion is given by $\Qn (\bv q)=\doubleunderline 1$. The ``Numerics'' curves were calculated as explained in section \ref{sec:stuartnum} and \ref{sec:scheme}.  The ``Gaspard'' curve was calculated as described in section \ref{sec:gaspard}. The ``Theory'' curve is given by \eqref{eq:R_myint} with the one-dimensional projections \eqref{eq:Fs} and \eqref{eq:Cs}.}
    \label{fig:stuart_numerics}
\end{figure}

\section{Brusselator}\label{sec:brussel_num}
The force and diffusion was given by \eqref{eq:brussel_FQ}. $p^0(\bv q)$ is the steady-state distribution of a Brusselator in the deterministic limit $\epsilon\rightarrow 0$.
Time-evolving $\bv q=(0,0)^T$ according to
\eq{
    \partial_t \bv q(t) &= \bv F(\bv q(t))\,,
}
for a large, ideally infinite time, we found a point $\bv q_\text{lc}$ on the limit cycle. Every time we wanted to simulate a trajectory, we calculated $\bv q(0)$ by time evolving $\bv q_\text{lc}$ according to the equation above for a time $T_0$. $T_0$ was drawn from a uniform distribution between $0$ and the period of the deterministic Brusselator.
Both $f(\bv q)$ and $g(\bv q)$ was defined to be the $x$ component of $\bv q$, resulting in
\eq{
    \cor(t) &= \lang x(t)x(0) \rang - \overline{\lang x(\tau) x(0)\rang}\,,
}
which is the autocorrelation function of $x$, shifted by a constant.
The result of this method alongside Gaspard's method, our theory \eqref{eq:R_myint} and the bound \eqref{eq:fcont}, both with the one-dimensional projections \eqref{eq:Fs} and \eqref{eq:Cs} are shown in figure \ref{fig:brussel_numerics}. We also calculated Gaspard's method and our numerics method without radial diffusion. To remove radial diffusion, we used the modified noise strength
\eq{\label{eq:Ctang}
    \doubleunderline C^\text{tang}(\bv q) &= \left| \Cn^T(\bv q)\bv e_F \right| \bv e_F(\bv q) \otimes \bv e_F(\bv q) \,,
}
with
\eq{
    \bv e_F(\bv q) &= \frac{\bv F(\bv q)}{\left| \bv F(\bv q) \right|}\,,
}
where $\Cn$ is the original noise strength of the Brusselator given by \eqref{eq:brussel_FQ} with \eqref{eq:Q=C}.
For both models, our numerics agree with Gaspard's method, except for $\epsilon=10^{-2}$ and low $\Delta\mu$.
This is probably because the low robustness results in rapid decaying peaks making it difficult to differentiate between peaks and numerical errors.
Another reason could be that Gaspard's calculations are approximations for small $\epsilon$.
Our theory agrees with Gaspard's method without radial diffusion. This is expected as our theory neglects phase-coupling caused by radial diffusion and \eqref{eq:Ctang} includes no radial diffusion.
Since phase-coupling increases the diffusion, the robustness is higher without radial diffusion.
In this figure, the upper bound \eqref{eq:fcont} is higher than \eqref{eq:R_myint} as proven in chapter \ref{sec:bound}.
Therefore \eqref{eq:fcont} is also an upper bound for the robustness of the full dynamics.

\begin{figure}
    \includegraphics{plots/brussel_numerics.pdf}
    \caption{Robustness of the Brusselator for different thermodynamic forces $\Delta\mu$ calculated using different methods. The ``Numerics'' curves were calculated as described in section \ref{sec:brussel_num} and \ref{sec:scheme}. The ``Gaspard'' curves were calculated as described in section \ref{sec:gaspard}. The ``Theory'' and ``Upper boundary'' curves are given by \eqref{eq:R_myint} and \eqref{eq:fcont}, both with the one-dimensional projections \eqref{eq:Fs} and \eqref{eq:Cs}. ``Tang'' indicates that the modified $\Cn^\text{tang}$ from \eqref{eq:Ctang} was used as the noise strength.}
    \label{fig:brussel_numerics}
\end{figure}
