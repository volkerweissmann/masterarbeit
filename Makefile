.PHONY:	clean

main.pdf: plots/*.pdf plots/example_cos1D.pdf main.tex normal_hopf.myout Langevin.myout numerics.tex integral.myout hopf_bifurc.myout 1D_robustness.tex appendix.tex eigenvals.tex brüssel.tex 2D_robustness.tex intro.tex theory.tex bound.tex chemical.tex src.bib summary.tex
	latexmk main.tex -quiet -pdf
	# texfot pdflatex -interaction=nonstopmode -file-line-error -halt-on-error main.tex
	# texfot pdflatex -interaction=nonstopmode -file-line-error -halt-on-error main.tex

rustsrc := rsimu/src/main.rs $(wildcard rsimu/src/*/mod.rs)

# cargo run --bin embedded_tex -- /home/volker/Sync/DatenVolker/git/Masterarbeit/src/normal_hopf.tex

%.myout: %.tex
	~/Sync/itp2/math_helper/eqn_server/target/debug/embedded_tex $<

plots:
	mkdir plots

plots/example_cos1D.pdf: plots
	echo "fake rebuild"
	#scripts/small.py rebuild

watch: main.pdf
	date +"%T"
	#pdfswitch
	#touch watch

clean:
	latexmk -c main
	rm main.pdf | true
	rm *.myout | true
	# rm "main.aux" | true
	# rm "main.toc" | true
	# rm "main.out" | true
	# rm "main.log" | true
	# rm "main.bbl" | true
	# rm "main.bcf" | true
	# rm "main.dvi" | true
