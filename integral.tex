\chapter{One-dimensional weak noise limit}\label{sec:integral}
% \eq{\label{eq:fokker_L}
%     \partial_t p( q,t) &= \hat L_{ q} p( q,t)
% }
% \eq{\label{eq:contLND_def}
%     \hat L_{ q} := \sum_i \lou -\ptdiff{q_i} F_i( q) - F_i( q)\ptdiff{q_i} + \sum_j \lou \epsilon\ptdiff{q_i}\ptdiff{q_j} Q_{ij}( q) + 2\epsilon \ptdiff{q_i}Q_{ij}( q)\ptdiff{q_j} + \epsilon Q( q)\ptdiff{q_i}\ptdiff{q_j} \rou\rou\,.
% }
% \eq{\label{eq:spectime}
%     p( q,t) &= \sum_k c_k e^{\lambda_k t} v_k( q)\,,
% }
As explained in the last chapter, the robustness is given by
\eq{\label{eq:Rdef}
    R = \frac{\omega}{\gamma}\,
}
where
\eq{\label{eq:lambdasplit}
    \lambda &= \pm i \omega -\gamma
}
is the first nontrivial eigenvalue of the Fokker-Planck operator. We dedicate this chapter to a case where $\omega$ and $\gamma$ can be calculated analytically: A one-dimensional Langevin system with periodic boundary conditions in the weak noise limit $\epsilon\ll 1$.

\section{Finding the eigenvalues}\label{sec:findeigvals}
The one-dimensional Fokker-Planck operator \eqref{eq:L_fokker} is
\eq{\label{eq:contL1D_def}
    \FPO &= \lou -F'(q) + \epsilon Q''(q)\rou  + \lou - F(q) + 2\epsilon Q'(q)\rou \partial_q + \epsilon Q(q) \partial_q^2\,.
}
Employing the same ansatz
\keqn{tex[\label{eq:gphidef}]
    phi[q] := -epsilon ln[v[q]]
    ---
    g[q] := D_q phi(q)
}
\eq{\label{eq:gphidef}
    \phi(q) \defin -\epsilon\ln v(q)\,,
}
as Gaspard \cite{Gaspard2002}, we find the eigenvalue equation
\eq{
    \FPO v_k( q) = \lambda_k v_k(q)
}
to become an explicit ordinary differential equation of order 2 for $g(q) \equiv \phi'(q)$\,,
\keqn{tex[\label{eq:fpg}]
    0 = (Q[q]g[q]^2 + F[q]g[q]) - epsilon(Q[q] D_q g[q] + 2 g[q]D_q Q[q] + D_q F[q] + lambda) + epsilon^2 D_q D_q Q[q]
}
\eq{\label{eq:fpg}
    0 &= (Q(q)g^2(q) + F(q)g(q)) - \epsilon(Q(q)g'(q)+ 2Q'(q)g(q) + F'(q) + \lambda) + \epsilon^2 Q''(q)\,.
}
The first solution is
\keqn{tex[\label{eq:g_a}]
    g[q] = 0 + epsilon (lambda + D_q F[q])/( F[q]) tex[\\&]
    + epsilon^2/F[q]^3 (-lambda^2 Q[q] - 3 lambda Q[q] D_q F[q] + 2 lambda F[q] D_q Q[q]) tex[\\&]
    + epsilon^2/F[q]^3 ( Q[q] F[q] D_q D_q F[q] - 2 Q[q] (D_q F[q])^2 - F[q]^2 D_q D_q Q[q] + 2 F[q] (D_q Q[q]) D_q F[q])
    tex[\\&+\mathcal O(\epsilon^3)]
}
\eq{\label{eq:g_a}
    g(q) &= 0 + \epsilon\frac{\lambda + F'(q)}{F(q)}\\
    &+ \epsilon^{2}/F^{3}(q) \lou - \lambda^{2} Q(q) - 3 \lambda Q(q) F'(q) + 2 \lambda F(q) Q'(q) \rou\\
    &+\epsilon^{2}/F^{3}(q) \lou Q(q) F(q)  F''(q) - 2 Q(q) F'(q)^2 - F^{2}(q)  Q''(q) + 2 F(q) Q'(q) F'(q)\rou\\
    &+\mathcal O(\epsilon^3)\,.
}
There exists a second solution but it leads to eigenvalues other than the first nontrivial one as explained in section \ref{sec:gb_sol}. We will therefore neglect it. Eq. \eqref{eq:g_a} suggests that every $\lambda\in\mathbb C$ is an eigenvalue of $\FPO$ but we have not fulfilled the periodic boundary conditions $v(q+\per)=v(q)$ yet. With \eqref{eq:gphidef}, they become
\eq{
    2\pi ki &= \frac{1}{\epsilon}\int_0^\per g(q)\diff{q}\,,
}
with $k\in\mathbb Z$. Inserting \eqref{eq:g_a}, we find a quadratic equation for $\lambda$\,,
\eq{
    0 &= \lambda^2 \int_0^\per\diff{q} \frac{\epsilon Q(q)}{F^3(q)} + \lambda\int_0^\per \diff{q} \frac{-\epsilon Q'(q)}{2F^2(q)} - \frac{1}{F(q)} - 2\pi ki\,.
}
In the weak noise limit, its two solutions are
\eq{
    \lambda_k =& i\lou 2\pi k\rou \lou\int_0^\per\frac{1}{F(q)}\diff{q}\rou^{-1} + i\mathcal O(\epsilon)\\
               & - \epsilon (2\pi k)^2 \lou \int_0^\per \frac 1{F(q)}\diff{q}\rou^{-3} \lou \int_0^\per \frac {Q(q)}{F^3(q)}\diff{q}\rou + \mathcal O(\epsilon^2)
}
and
\eq{
    \lambda_k' =& -i\lou 2\pi k\rou \lou\int_0^\per\frac{1}{F(q)}\diff{q}\rou^{-1} + i\mathcal O(\epsilon)\\
    &- \frac 1\epsilon \lou \int_0^\per \frac 1{F(q)}\diff{q}\rou \lou \int_0^\per \frac {Q(q)}{F^3(q)}\diff{q}\rou^{-1} + \mathcal O(\epsilon^0)\,.
}
% \eq{\label{eq:myint}
%     \omega_k &= \lou 2\pi k\rou \lou\int_0^\per\frac{1}{F(q)}\diff{q}\rou^{-1} + \mathcal O(\epsilon)\\
%     \gamma_k &= \epsilon (2\pi k)^2 \lou \int_0^\per \frac 1{F(q)}\diff{q}\rou^{-3} \lou \int_0^\per \frac {Q(q)}{F^3(q)}\diff{q}\rou + \mathcal O(\epsilon^2)
% }
% and
% \eq{
%     \omega_k' &= -\lou 2\pi k\rou \lou\int_0^\per\frac{1}{F(q)}\diff{q}\rou^{-1} + \mathcal O(\epsilon)\\
%     \gamma_k' &= \frac 1\epsilon \lou \int_0^\per \frac 1{F(q)}\diff{q}\rou \lou \int_0^\per \frac {Q(q)}{F^3(q)}\diff{q}\rou^{-1} + \mathcal O(\epsilon^0)\,,
% }
%where we have again split $\lambda$ into its imaginary part $\omega$ and its negative real part $\gamma$.
$\lambda_{\pm 1}$ is the first nontrivial eigenvalue if $\epsilon\ll 1$. Using \eqref{eq:lambdasplit} and \eqref{eq:Rdef} we find
\eq{\label{eq:R_myint}
    \omega &= \lou 2\pi k\rou \lou\int_0^\per\frac{1}{F(q)}\diff{q}\rou^{-1} + \mathcal O(\epsilon)\\
    \gamma &= \epsilon (2\pi k)^2 \lou \int_0^\per \frac 1{F(q)}\diff{q}\rou^{-3} \lou \int_0^\per \frac {Q(q)}{F^3(q)}\diff{q}\rou + \mathcal O(\epsilon^2)\\
    R &= \frac{1}{2\pi\epsilon} \lou \int_0^\per \frac 1{F(q)}\diff{q}\rou^{2} \lou \int_0^\per \frac {Q(q)}{F^3(q)}\diff{q}\rou^{-1} + \mathcal O(\epsilon^0)\,,
}
the main result of this thesis. If we assume $F(q)>0$ and $Q(q)>0$, all of these integrals exist and yield a finite value. We will discuss this assumption in section \ref{sec:F>0Q>0}.
\blue{
\section{Periodic integrals}
\subsection{b}
\eq{
    \partial_q F^a Q^b &= a F^{a-1} F' Q^b + F^a b Q^{b-1} Q'
}
\eq{
    \partial_q\lou \frac{3}{2} F^{-2} Q \rou &= -3 F^{-3} F' Q + \frac{3}{2} F^{-2} Q'
}
\eq{
    \partial_q F^{-2} F' Q = F^{-2} F' Q' + F^{-2} F'' Q - 2 F^{-3} F'^2 Q = F^{-3}\lou F F' Q' + F F'' Q - 2 F'^2 Q \rou
}
\eq{
    \partial_q\lou -F^{-1} Q' + F^{-2} F' Q\rou &= F^{-3}\lou 2 F F' Q' - F^{2} Q'' + F F'' Q - 2 F'^2 Q\rou
}
}
