\chapter{From eigenvalues to oscillations}\label{sec:eigvals}
In the last chapter, we made it plausible that Langevin system can show damped oscillations. We presented a simple one-dimensional system with constant force and diffusion and saw that
\eq{
    \lang x(t) \rang &= \lang\cos\lou\frac{2\pi}{\FPO} q(t)\rou\rang
}
shows damped oscillations. In this chapter, we will prove that
\eq{\label{eq:fgdamped}
    \lang f(\bv q(t))g(\bv q(0)) \rang = a + b \cos\lou \omega t + \varphi \rou e^{-t\gamma} + h(t)\,,
}
where $a,b,\varphi\in\mathbb R$, $\omega,\gamma\in\mathbb R^+$, $f,g \in \mathbb R^n\rightarrow\mathbb R$ and $h(t)$ is a function that decays faster than $e^{-t\gamma}$, is a general result for many $N$-dimensional Fokker-Planck systems. We will also show that $\omega$ and $\gamma$ are given by the imaginary and negative real part of the first nontrivial eigenvalue of the Fokker-Planck operator, which is crucial since we are interested in the robustness, defined as
\eq{
    R &= \frac{\omega}{\gamma}\,.
}
Finding $\omega$ and $\gamma$ is therefore an important goal.
In chapter \ref{sec:integral} we will do this by calculating the eigenvalues of $\FPO$. In chapter \ref{sec:numerics} we will do this by numerically finding the peaks of \eqref{eq:fgdamped}. Gaspard (see section \ref{sec:gaspard}) did this analytically using Hamilton-Jacobi theory.

I am not the first one to find that correlation functions show damped oscillations, where the angular frequency is the imaginary part of the first nontrivial eigenvalue and the damping strength is the real part of the first nontrivial eigenvalue \cite{Barato2017, Gaspard2002, The_correlation_time_of_mesoscopic_chemical_clocks, Theoretical_study_of_robustness_factors}.

\section{Spectral decomposition}
Assuming that $\FPO$ is diagonalizable (see section \ref{sec:jordan}) the solution to the Fokker-Planck equation \eqref{eq:fokker} is \cite{Gaspard2002}
\eq{\label{eq:pxt_eigval}
    p(\bv q,t|\bv q_0) &= e^{\FPO t}\delta(\bv q-\bv q_0)
    \\ &= \sum_k c_k(\bv q_0) e^{\lambda_k t} v_k(\bv q)\,,
}
where $v_k(\bv q)$ are the eigenfunctions of $\FPO$, i.e.
\eq{\label{eq:eigval}
    \lambda_k v_k(\bv q) &= \FPO v_k(\bv q)
}
and
\eq{\label{eq:find_c_k}
    c_k &= v_k(\bv q_0) \bigg/ \int_0^\per \diff{\bv q} \left| v_k(\bv q) \right|^2 \,.
}
\blue{
    \eq{
        \int_0^\per \diff{ q} \overline p( q,0) v_k( q) &= c_k \int_0^\per |v_k( q)|^2
        \\ v_k( q_0) &=  c_k \int_0^\per |v_k( q)|^2
    }
}
Splitting $\lambda_k$ into an imaginary part $\omega_k$ and a negative real part $\gamma_k$, \eqref{eq:pxt_eigval} becomes
\eq{\label{pxt_sum_omega_gamma}
    p(\bv q,t|\bv q_0) &= \sum_k c_k(\bv q_0) e^{i\omega_k t} e^{-\gamma_k t} v_k(\bv q)\,.
}
We will later see how $e^{i\omega_k t}$ results in the oscillations from \eqref{eq:fgdamped} and $e^{-\gamma_k t}$ results in the damping from \eqref{eq:fgdamped}.

\section{First nontrivial eigenvalue}\label{sec:firstnontrivial}
If we draw our initial state from an arbitrary distribution $p^0(\bv q)$, and use \eqref{eq:pxt_eigval} for time-evolution, we find
\eq{\label{eq:akdef_etal}
    \lang f(\bv q(t))g(\bv q(0)) \rang &= \int\diff{\bv q_0}\int\diff{\bv q_1} f(\bv q_1)g(\bv q_0) p^0(\bv q_0) p(\bv q_1,t |\bv q_0)
    \\ &= \int\diff{\bv q_0}\int\diff{\bv q_1} f(\bv q_1)g(\bv q_0) p^0(\bv q_0) \sum_k e^{i\lambda_k t} c_k(\bv q_0) v_k(\bv q_1)
    \\ &= \sum_k e^{i\lambda_k t} \underbrace{\int\diff{\bv q_0}\int\diff{\bv q_1} f(\bv q_1)g(\bv q_0) p^0(\bv q_0) c_k(\bv q_0) v_k(\bv q_1)}_{\equiv a_k}
}
\eq{\label{eq:eigvals18}
    \lang f(\bv q(t))g(\bv q(0)) \rang &= \sum_k e^{\lambda_k t} a_k\,.
}
The $e^{\lambda_k t}$ that showed up in \eqref{eq:pxt_eigval} shows here up too. The $a_k$'s are real numbers, independent of $t$, but dependent on $f,g,p^0$. Our goal is to find the $e^{\lambda_k t} a_k$ terms with the highest absolute value, because the other ones can be neglected, greatly simplifying the expression. Since we are interested in the long-time limit, the most relevant terms are the ones with a non-zero $a_k$ and a high $\Re(\lambda_k)=-\gamma_k$.\\
If the conditions
\begin{itemize}
    \item $\lambda_0=0$
    \item $\gamma_k > 0 \forall k\neq 0$
    \item $\gamma_1 = \gamma_{-1}$
    \item $\gamma_k > \gamma_1 \forall k \neq 0,1,-1$
    \item $\omega_1 = -\omega_{-1} \neq 0$
    \item $a_1 = a_{-1}^* \neq 0$
    ,
\end{itemize}
are met, we can ignore all $e^{\lambda_k t} a_k$'s except $k=0,1,-1$ in the long time limit to get
\eq{
    \lang f(\bv q(t))g(\bv q(0)) \rang &= a_0 e^{\lambda_0 t} +  a_1 e^{\lambda_1 t} + a_{-1} e^{\lambda_1 t}
    \\ &= a_0 + 2|a_1| \cos\lou\omega_1 t + \varphi(a_1)\rou e^{-\gamma_1 t} \,,
}
where $\varphi(a_1)$ is the phase of $a_1$, i.e.
\eq{
    a_1 = |a_1|e^{i\varphi(a_1)}\,.
}
This has the same form as \eqref{eq:fgdamped}. So all there is left to proof are the conditions from the list above. This will be done in the following two sections.

\section{Proving $\lambda_0=0$ and $\gamma_k > 0 \forall k\neq 0$}\label{sec:lambdaproof}
For any real Matrix $\doubleunderline L$ with
\eq{\label{eq:keizercond}
    L_{ij} &\geq 0 \quad \forall i\neq j\\
    \sum_i L_{ij} &= 0\,
}
%afterwards warum steht da im Original $ij$ statt $ii$?
there is at least one eigenvector with a vanishing eigenvalue. If the graph of $\doubleunderline L$ is strongly connected, there is exactly one eigenvector with a vanishing eigenvalue \cite{Keizer1972, Qian2000}.
%All other eigenvalues have negative real parts. And all other eigenvectors $v$ fulfill
% \eq{
%     \sum_i v_i = 0\,.
% }
This applies to the Fokker-Planck operator, since discretization leads to the transition rate matrix from the master equation \eqref{eq:master} \cite{VANKAMPEN2007}. Hence
\eq{
    \lou \gamma_k = 0 \land \omega_k = 0 \rou \lor \gamma_k > 0\,.
}
The graph of the discretized Fokker-Planck operator is strongly connected, thus there is only one vanishing eigenvalue. Since we are free to reorder the $k$-numbering, we choose $\lambda_0=0$. This proofs the first two conditions from our list.

\section{Fulfilling the other eigenvalue conditions}\label{sec:otherconds}
By taking the complex conjugate of \eqref{eq:eigval} we get
\eq{
    \lambda_k^* v_k^*(q) &= \FPO v_k^*(q)\,.
}
We therefore know that the non-real eigenfunctions and eigenvalues come in pairs that are complex conjugated.
By taking the complex conjugate of \eqref{eq:find_c_k}, we can see that $c_k$'s belonging to a such a pair are complex conjugated and the $a_k$'s are therefore complex conjugated too, since $f,g$ and $p^0$ are real.

Let us order all eigenvalues with $a_k\neq 0$ by their real part and look at the second place, i.e. the eigenvalue with the highest non-zero real part, also called the first nontrivial eigenvalue.
For the purpose of this thesis, we assume that a single pair of complex conjugates takes the second place. Then we can fulfill all our conditions from the list by naming this pair of complex conjugates $\lambda_1$ and $\lambda_{-1}$.
An instance of this assumption breaking is if $f$ is a constant, independent of $\bv q$. Then all $a_k$'s, except for $a_0$ vanish and no eigenvalue takes the second place.
Further research is required to determine when this assumption holds true.
%If not, .\footnote{If e.g. the eigenvalue with the highest non-zero real part has $a_k=0$, then $\omega$ and $\gamma$ might differ depending on the definition of $f,g,p^0$, since changing $f,g,p^0$ can change which $a_k$'s vanish.}
%I did not find a case where this problem occurs, except if you define $f$ and $g$ in a way that all $a_k$, except for $a_0$ vanish and no eigenvalue takes the second place. You can do this by e.g. defining $f$ to be a constant, independent of $\bv q$.\\
%The fact that in the papers I read \cite{Barato2017, Gaspard2002, The_correlation_time_of_mesoscopic_chemical_clocks} and the discussions I had with colleagues, everyone just talked about the "first nontrivial eigenvalue" is an indication that these problems are not a common big problem.

\section{Revisiting the case of constant force and diffusion}
In chapter \ref{sec:1D_robustness} we looked at the ensemble average of the observable
\eq{
    x(q)\equiv\cos\lou \frac{2\pi}{\per} q\rou
}
in a one-dimensional system with $q(0)=0$, $F(q)=F_0$, $Q(q)=Q_0$ and found
\eq{
    \lang x(t)\rang = \cos\lou\frac{2\pi}\per F_0  t\rou \exp\lou - t\lou\frac{2\pi}{\per}\rou^2\epsilon Q_0 \rou\,.
}
Applying the spectral decomposition method presented in this chapter (see section \ref{sec:constspec}), we find
\eq{
    \lang x(t) \rang &= \sum_{k=1,-1} \frac 1 2 \exp\lou - i F_0 k \frac{2\pi}{\per} t- \epsilon Q_0 \lou k\frac{2\pi}{\per}\rou^2 t\rou
    %\\ &= \cos\lou \frac{2\pi}{\per} F_0 t \rou \exp\lou - \epsilon Q_0 \lou \frac{2\pi}{\per}\rou^2 t\rou
    \,,
}
which is \emph{exactly} equal.
In section \ref{sec:firstnontrivial} we argued how all terms with $|k|>1$ can be neglected as they fall of faster than the $k=\pm 1$ terms. In this simple case they vanish, hence $h(t)=0$ and our approximations are not needed.

%\eqref{eq:const_cosexp}

% This is an exact statement and does not use the approximations we introduced in this chapter, but it looks like the result we found in this chapter. The reason for this is that all these approximations are exact if we look at this particular system.
% In section \ref{sec:firstnontrivial}, we showed that $e^{\lambda_k t} a_k$ terms with $k\neq 0,1,-1$ can be neglected. But in this case, these terms vanish, so the approximation is exact. If we insert everything we found in this section into \eqref{eq:eigvals18}, we get
% \eq{
%     \lang x(q(t)) \rang &= \sum_{k=1,-1} \frac 1 2 \exp\lou - i F_0 k \frac{2\pi}{\per} t- \epsilon Q_0 \lou k\frac{2\pi}{\per}\rou^2 t\rou
%     \\ &= \cos\lou \frac{2\pi}{\per} F_0 t \rou \exp\lou - \epsilon Q_0 \lou \frac{2\pi}{\per}\rou^2 t\rou
%     \,,
% }
% which is exactly \eqref{eq:const_cosexp}.
