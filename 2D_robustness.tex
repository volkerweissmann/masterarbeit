\chapter{One-dimensional effective description of two-dimensional Langevin systems}\label{sec:2Drobust}
In chapter \ref{sec:hopf}, we looked at two-dimensional systems that can be described using
\eq{\label{eq:2D_det_2}
    \partial_t\bm q(t) = \bm F(\bm q(t))
}
and show a limit cycle, in particular the Stuart-Landau oscillator \eqref{eq:stuart}. In chapter \ref{sec:1D_robustness} we looked at one-dimensional systems that can be described using the Langevin equation
\eq{
    \partial_t q(t) &= F(q(t)) + \sqrt{\epsilon} C(q(t))\zeta(t)
}
and show damped oscillations in observables. In particular we looked at the case of constant $F$ and $C$ where we saw that
\eq{\label{eq:const_cosexp_nlc}
    \lang x(t)\rang \equiv \lang \cos\lou\frac{2\pi }{\per} q(t) \rou \rang = \cos\lou\frac{2\pi}LF_0  t\rou \exp\lou - t\lou\frac{2\pi}{\per}\rou^2\epsilon C_0^2/2 \rou \,.
}
In this chapter, we look at the combination of both: If we add noise to \eqref{eq:2D_det_2}, we get
the $N$-dimensional Langevin equation \eqref{eq:lange_ND}.
Since the limit cycle is an attractor, it continues to exist in the presence of noise. Similar to the example from section \ref{sec:1D_robustness}, such systems also show damped oscillations. As an example, we look at the system we used to introduce supercritical Hopf bifurcations in section \ref{sec:hopf}: The Stuart-Landau oscillator. The system follows \eqref{eq:lange_ND} where $\bm F(\bm q)$ is given by \eqref{eq:stuart}. Setting $\epsilon = 0$ leads back to the deterministic system from section \ref{sec:hopf}. If we set
\eq{
    \Cn(\bm q) &= \begin{pmatrix}
       -y & 0 \\
       x & 0
    \end{pmatrix}\,,
}
i.e. the diffusion only happens tangential to $\Fnt$, the system is equivalent (except for rescaling) to the constant-force, constant-diffusion system discussed in section \ref{sec:1D_robustness}. Analogous to \eqref{eq:const_cosexp_nlc} we get
\eq{
    \lang x(t)\rang = r_0\cos\lou\frac{F_0}{r_0}  t\rou \exp\lou - t\epsilon r_0^2 /2  \rou \,,
}
with $r_0=\sqrt{a/b}$. For a general $\Cnt$, the two-dimensional system is effectively a one-dimensional system, in the weak noise limit $\epsilon\ll 1$. Since the limit cycle is a stable attractor, there is a restoring force counteracting the radial variance. As $t\rightarrow\infty$ radial variance approaches a finite value that is proportional to $\epsilon$. In other words, the system is effectively confined into a narrow periodic band. An ensemble of 20 systems at different times is shown in figure \ref{fig:example_grouping}. The systems do not lie exactly on the limit cycle, but in its proximity. This allows us to effectively describe two-dimensional Langevin systems as a one-dimensional system \cite{louca2018stable}. We can thus apply our knowledge gained in section \ref{sec:integral} on two-dimensional systems. We begin by defining $\bv q_\text{lc}(s)$ to be the arc-length parameterization of the limit cycle. Defining
\eq{
    \bv e_F(s) &= \frac{\bv F(\bv q(s))}{\left| \bv F(\bv q(s)) \right|}\,,
}
the tangential component of the force is given by
\eq{\label{eq:Fs}
    F(s) &= \bv e_F(s) \bv F(\bv q(s))
}
and the tangential component of the diffusion is given by
\eq{\label{eq:Cs}
    C(s) &= \left| \Cn^T(\bv q(s))\bv e_F(s) \right|\,.
}
If we insert this into \eqref{eq:R_myint} we find a robustness. But this robustness is only an upper bound because the radial component of the diffusion indirectly leads to tangential diffusion. For our example of the Stuart-Landau oscillator, the deterministic component of the angular velocity is
\eq{
    F_\varphi(r, \varphi) &= c + d r^2
}
and therefore $r$-dependent. Thus, the realizations that have a higher $r$ due to radial noise overtake the realizations that have a lower $r$ due to radial noise. This leads to a difference in $\varphi$ between different realizations. This is called phase-coupling and is present in the full, two-dimensional dynamics, but neglected in the one-dimensional description.

\begin{figure}
    \includegraphics{plots/example_grouping.pdf}
    \caption{Time evolution of 20 two-dimensional systems under \eqref{eq:lange_ND}. $\bv F(\bv q)$ is given by \eqref{eq:stuart} with $a=b=c=d=1$. $\sqrt{\epsilon}\Cn(\bv q)=\doubleunderline 1/10$. The radial variance rises quickly, but because the limit cycle is an attractor it stays small. This figure looks similar to figure \ref{fig:example_1D_ring} since a noisy system with a limit cycle can be effectively described using one-dimensional dynamics. The angular diffusion is twice as big, because of phase-coupling.}
    \label{fig:example_grouping}
\end{figure}
