\chapter{Conclusion}
In chapter \ref{sec:chem} we looked at chemical networks. We saw that they have inherit noise and are described by a master equation. Approximating that the concentration stays roughly constant during a small time $\Delta t$, we saw that chemical reactions are a poisson process. Since the poisson distribution becomes a gaussian distribution in the continuum limit, large chemical networks follow Langevin dynamics.
While others have proven the same \cite{doi:10.1063/1.481811}, our explanation is more intuitive than their rather formal approach.

This work was about stochastic systems following the Langevin dynamics or the master equation. These dynamics are explained in chapter \ref{sec:stochasticdyn}.
Systems following Langevin dynamics or the master equation can exhibit noisy oscillations. In chapter \ref{sec:1D_robustness} we explain that the ensemble average of an observable shows damped oscillations because the realizations dephase. The robustness is defined by the ratio of the angular frequency and the damping strength.
It is also a measure of the time-precision of the oscillator. This is important, since living systems use such oscillators to time other processes. For many systems, calculating the robustness is quite difficult. For a system with only 3 discrete states, there is a simple analytical expression for the robustness \cite{Barato2017}. For discrete systems with more states, Barato and Seifert \cite{Barato2017} recently conjectured an upper bound. For continuous systems with weak noise, Gaspard \cite{Gaspard2002} found an analytical expression of the robustness. But his method is quite involved, so further research is required to find simpler expressions. A numerical method of calculating the robustness is described in section \ref{sec:scheme}: We numerically generated Langevin-trajectories and calculated the ensemble average of an observable. The quality factor of this damped oscillation is the robustness. Since these numerics are quite time-intensive, analytical progress toward better methods is welcome.

In chapter \ref{sec:eigvals} we proved that certain Langevin systems show damped oscillations and connected their robustness to the first nontrivial eigenvalue of the Fokker-Planck operator, although further research is required to verify our assumptions in section \ref{sec:otherconds}. Our main research is presented in chapter \ref{sec:integral}. Using a similar ansatz as Gaspard \cite{Gaspard2002} we found the first nontrivial eigenvalue of the one-dimensional Fokker-Planck operator in the weak noise limit. We thus arrive at our main result:
\begin{mybox}
    The robustness $R$ of a one-dimensional, $P$-periodic system described by the  Fokker-Planck equation
    \eq{
        \partial_t p(q,t) &= - \partial_q \lou F(q) p(q,t) \rou + \epsilon \partial_q^2\lou  Q(q)  p(q,t) \rou
    }
    is given by
    \eq{\label{eq:conclusion_R}
        R &= \frac{1}{2\pi\epsilon} \lou \int_0^\per \frac 1{F(q)}\diff{q}\rou^{2} \lou \int_0^\per \frac {Q(q)}{F^3(q)}\diff{q}\rou^{-1} + \mathcal O(\epsilon^0)\,.
    }
\end{mybox}
Since we focussed on the weak noise limit $\epsilon\ll 1$, further research is required to calculate higher $\epsilon$-orders.
In chapter \ref{sec:bound} we performed the continuum limit of the upper bound conjectured by Barato and Seifert \cite{Barato2017}, arriving at
\eq{\label{eq:conclusion_bound}
    R\leq f(\affinity) &= \frac 1{2\pi\epsilon} \int_0^\per \frac{F(q)}{Q(q)}\,.
}
We proved that \eqref{eq:conclusion_R} lies below this bound for any system.

The concept of supercritical Hopf bifurcations is explained in chapter \ref{sec:hopf}.
In chapter \ref{sec:2Drobust} we showed how, in the weak noise limit, a two-dimensional Langevin system can be effectively described using a one-dimensional Langevin equation. Essentially, the phase space can be resctricted to the limit cycle, which is a one-dimensional structure. This allows us to apply \eqref{eq:conclusion_R}, but the result is only an upper bound since phase-coupling is ignored by our approach. Further research is required to calculate the contribution of phase-coupling for complex systems.
Comparing \eqref{eq:conclusion_R} with Gaspard's result of
\eq{
    R = \frac{T^2}{\pi\epsilon|\partial T/\partial E|}\,,
}
% and recall that
% \eq{
%     \int_0^\per \frac 1{F(q)}\diff{q}
% }
% is the deterministic period $T$,
we find
\eq{
    \frac{\partial T}{\partial E} &= 2\int_0^\per \frac {Q(s)}{F^3(s)}\diff{s} \,,
}
with $F(s)$ and $Q(s)$ given by \eqref{eq:Fs} and \eqref{eq:Cs} with \eqref{eq:Q=C}.
Further research is required to understand this connection.

In chapter \ref{sec:brussel} we looked at the Brusselator, a chemical network, and calculated the bifurcation point in section \ref{sec:find_muc} to be  $\Delta\mu_c=3.93$. Further research is required to understand the deviation from Basile's value of $\Delta\mu_c=3.95$ \cite{basile}.

In the last chapter, we compared our theoretical predictions of previous chapters to numerical data. We saw good agreement, indicating that our findings are valid. In particular, we looked at a one-dimensional system with
\eq{
    F(q) &= 1+a\sin (q)\\
    Q(q) &= 1\,.
}
The numerical data agrees nicely with \eqref{eq:conclusion_R}. For this system, the data confirmed the validity of the bound \eqref{eq:conclusion_bound}. We also looked at the Stuart-Landau oscillator and confirmed Gaspard's \cite{Gaspard2002} method. We found our one-dimensional effective dynamics to produce the same robustness, if and only if phase-coupling can be neglected. Otherwise, the full dynamics yield a lower robustness. The last system of interest was the Brusselator. The numerics largely agreed with Gaspard's method, although they seem a lot less precise. Again, we saw that the \eqref{eq:conclusion_R}  with the one-dimensional effective dynamics yields a higher robustness than the full dynamics. When the noise strength $\Cnt$ was modified to exclude radial diffusion, Gaspard's method matched \eqref{eq:conclusion_R}. Again, Barato and Seifert's bound was higher than the robustness, both with and without radial-diffusion.

\chapter{Zusammenfassung}
%\addcontentsline{toc}{chapter}{Zusammenfassung}

Das erste Kapitel dieser Arbeit bildet die Einleitung, in der wir zahlreiche Beispiele für Oszillatoren in der Natur nennen. Da die zu Grunde liegenden Prozesse stochastischer Natur sind, verhalten sich diese Oszillatoren nicht rein deterministisch. Dies ist für uns der Grund uns der Frage zu widmen, wie die Präzision solcher stochastischen Oszillatoren bestimmt werden kann. Diese Präzision wird durch die Robustness quantifiziert. Zu Schluss der Einleitung beschreiben wir den Aufbau dieser Arbeit. Für diejenigen die neu in der statistischen Mechanik sind, folgt in nächsten Kapitel eine kurze Erläuterung der wichtigsten beiden Dynamiken. Zahlreiche kontinuierliche stochastische Prozesse werden durch die Langevin-Gleichung oder die äquivalente Fokker-Planck-Gleichung beschrieben. Zahlreiche diskrete stochastische Prozesse werden durch die Mastergleichung beschrieben.

Das dritte Kapitel widmen wir den chemischen Netzwerken. Diese werden durch die Mastergleichung beschrieben, doch im Kontinuumslimes erhält man eine Langevin-Gleichung.
Im nächsten Kapitel erklären wir anschaulich wieso Observablen in Langevin-Systemen gedämpfte Oszillationen zeigen können. Das Verhältnis aus Kreisfrequenz und Abklingkonstante nennen wir Robustness oder Gütefaktor.
Im fünften Kapitel beweisen wir dieses Auftreten von gedämpften Oszillationen. Wir finden eine Verbindung zwischen dem ersten nichttrivialen Eigenwert und der Robustness.
Mit diesem Wissen im Hinterkopf widmen wir uns eindimensionalen Langevin-Systemen im sechsten Kapitel. Mit einem ähnlichen Ansatz wie Gaspard \cite{Gaspard2002} und einer Näherung für kleine Diffusionen gelingt es uns den ersten nichttrivialen Eigenwert zu finden. Damit erhalten wir unser wichtigstes Ergebnis:
\begin{mybox}
    Die Robustness $R$ eines eindimensionalen Systems mit der Periode $P$, welches durch die Fokker-Planck-Gleichung
    \eq{
        \partial_t p(q,t) &= - \partial_q \lou F(q) p(q,t) \rou + \epsilon \partial_q^2\lou  Q(q)  p(q,t) \rou
    }
    beschrieben wird ist
    \eq{\label{eq:zusammenfassung_R}
        R &= \frac{1}{2\pi\epsilon} \lou \int_0^\per \frac 1{F(q)}\diff{q}\rou^{2} \lou \int_0^\per \frac {Q(q)}{F^3(q)}\diff{q}\rou^{-1} + \mathcal O(\epsilon^0)\,.
    }
\end{mybox}
Für diskrete Systeme haben Barato und Seifert \cite{Barato2017} eine obere Schranke für die Robustness gefunden. Da die Mastergleichung im Kontinuumslimes zu einer Fokker-Planck-Gleichung wird, erhalten wir im siebten Kapitel
\eq{\label{eq:deutsch_bound}
    R\leq f(\affinity) &= \frac 1{2\pi\epsilon} \int_0^\per \frac{F(q)}{Q(q)}\,.
}
als eine obere Schranke für die Robustness von Langevin-Systemen. Wir zeigen analytisch, dass \eqref{eq:zusammenfassung_R} diese Schranke stets einhält.
Im achten Kapitel verlassen wir die statistische Mechanik und widmen uns deterministischen mehrdimensionalen Systemen. Wir erklären das Konzept eines superkritischen Hopf Bifurkation und das Konzept der Grenzzyklen anhand des Stuart-Landau Oszillators. Im nächsten Kapitel fügen wir thermisches Rauschen zu dem Stuart-Landau Oszillator hinzu und erhalten so eine Kombination aus den Phänomen der vierten und achten Kapitel. Das System bewegt sich in etwa auf einem Grenzzyklus, es treten jedoch Fluktuationen auf. Da sich das System stets in der Nähe des Grenzzykluses befindet ist es effektiv ein eindimensionales System. Wir können deshalb unser Wissen aus dem sechsten Kapitel anwenden. Die mit \eqref{eq:zusammenfassung_R} erhaltene Robustness ist jedoch nur eine obere Schranke, da die Phasenkopplung vernachlässigt wurde. Im zehnten Kapitel schauen wir uns den Brüsselator an, ein chemisches Netzwerk welches einen verrauschten Grenzzyklus hat. Wir berechnen den Bifurkationspunkt des Brüsselators. Im nächsten Kapitel vergleichen wir unsere theoretischen Vorhersagen mit numerischen Werten. Diese bestätigten weitestgehend unsere Theorie.
