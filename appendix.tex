\chapter{Appendix}
\section{Gaspard's method for calculating the robustness}\label{sec:gaspard}
Gaspard \cite{Gaspard2002} showed that the following method can be used to calculate the robustness of a system under Langevin dynamics in the weak-noise limit:
\begin{itemize}
    \item Find the limit cycle $\bv q_{\text{lc}}(t)$. Let the period be $T$.
    \item Solve the inital value problem
    \eq{
        \doubleunderline M(0) &= \doubleunderline 1\\
        \partial_t \doubleunderline M(t) &= \doubleunderline J(\bv q_{\text{lc}})(t)) \cdot \doubleunderline M(t)\,,
    }
    where $\doubleunderline J$ is the Jacobian of $\bv F$.
    \item Find the eigenvectors $\bv v_i$ and eigenvalues $\lambda_i$ of $\doubleunderline M^T(T)$. $\lambda_1$ should be $1$.
    \item Define
    \eq{
        \bv f_1 &= \frac{\bv v_1}{\bv v_1 \bv F(\bv q_{\text{lc}})(0))}
    }
    \item Solve the inital value problem
    \eq{
        \bv q(0) &= 0\\
        \bv p(0) &= f_1\\
        \partial_t \bv q(t) &= \doubleunderline J\lou\bv q_{\text{lc}}(t)\rou \bv q(t) + 2 \Qn(\bv q_{\text{lc}})(t)) \bv p(t) \\
        \partial_t \bv p(t) &= -\doubleunderline J^T\lou\bv q_{\text{lc}}(t)\rou \bv p(t)
    }
    \blue{
    \eq{
        \frac{dT}{dE} = -\bv f_1 \bv q(T)
    }}
    \item The Robustness is given by
    \eq{\label{eq:gaspard_R}
        R = \frac{T^2}{\epsilon\pi|\bv f_1 \bv q(T)|}\,.
    }
\end{itemize}

\section{Time evolution if force and diffusion are constant}\label{sec:const_cosexp}
In this section, we proof \eqref{eq:const_cosexp}. For constant force and diffusion, the Fokker-Planck equation \eqref{eq:fokker} becomes
\eq{
    \partial_t p(q, t) &= -F_0 \partial_q p(q,t) +  \epsilon C_0^2 /2 \partial_q^2 p(q,t)\,.
}
The solution to the differential equation above is
\eq{
    p(q,t) &= \frac{1}{\sqrt{\pi 4\epsilon Q_0 t}} \exp\lou \frac{-(q-F_0 t)^2}{4\epsilon Q_0 t} \rou\,,
}
if $p(q,0)=\delta(q)$, i.e. the particle is initially at $q=0$.
The ensemble average
\eq{
    \lang x(t)\rang &= \lang \cos\lou\frac{2\pi}{\per}q\rou\rang
}
is by definition
\eq{
    \lang x(t)\rang &= \int_{-\infty}^\infty \diff{q} \cos\lou\frac{2\pi}{\per}q\rou p(q,t)\,.
}
Splitting the $\cos$ function into
\eq{
    \frac 1 2 \lou e^{i\frac{2\pi}{\per}q} + e^{-i\frac{2\pi}{\per}q}\rou
}
and performing the integral yields
\eq{
    %&= \int_{-\infty}^\infty \diff{q} \frac 1 2 \lou e^{iq \cdot 2\pi/\per} + e^{-iq \cdot 2\pi/\per}\rou p(q,t)\\
    \lang x(t)\rang &= \cos\lou \frac{2\pi}LF_0 t\rou \exp\lou - t\lou\frac{2\pi}{\per}\rou^2\epsilon C_0^2/2 \rou
}

\section{Spectral decomposition if force and diffusion are constant}\label{sec:constspec}
The eigenfunctions of
\eq{
    \FPO &= - F_0 \partial_q + \epsilon Q_0 \partial_q^2
}
are
\eq{
    v(q) &= e^{ibq}\,,
}
with $b\in\mathbb R$, corresponding to the eigenvalue
\eq{
    \lambda &= - i F_0 b - \epsilon Q_0 b^2 \,.
}
The periodic boundary condition restricts this to
\eq{
    b &= k\cdot \frac{2\pi}{\per}\,,
}
with $k\in\mathbb Z$.%
\blue{
    \eq{
        0 &= \lou - \lambda - F_0 \partial_q + \epsilon Q_0 \partial_q^2\rou e^{ibq} \\
        0 &= -\lambda - F_0 ib - \epsilon Q_0 b^2\\
        \lambda &= - i F_0 b - \epsilon Q_0 b^2
    }
}
Using \eqref{eq:find_c_k} and $q_0=0$, we find $c_k$ to be
\eq{
    c_k &= 1 / \per \,.
}
Using \eqref{eq:akdef_etal}, $g(0)=1$ and $f(q)=x(q)$ we find
\eq{
    a_k &= \int\diff{ q_0}\int\diff{ q_1} f( q_1)g( q_0) p^0( q_0) c_k( q_0) v_k( q_1)
    \\ &= \int_{0}^\per \diff{q} \cos\lou \frac{2\pi}{\per}q\rou \exp\lou ik\frac{2\pi}{\per}q \rou /\per
    \\&= \begin{cases}
        1/2 & \text{for }k=1\\
        1/2 & \text{for }k=-1\\
        0 & \text{otherwise}
    \end{cases}\,.
}
\blue{
    \eq{
        &\quad\int_{0}^\per \diff{q} \cos\lou \frac{2\pi}{\per}q\rou \exp\lou ik\frac{2\pi}{\per}q \rou
        \\&=\frac{1}{2}\int_{0}^\per \diff{q} \lou \exp\lou i \frac{2\pi}{\per}q\rou + \exp\lou -i \frac{2\pi}{\per}q\rou \rou \exp\lou ik\frac{2\pi}{\per}q \rou
        \\&=\frac{1}{2}\int_{0}^\per \diff{q} \exp\lou i(k+1)\frac{2\pi}{\per}q \rou + \exp\lou i(k-1)\frac{2\pi}{\per}q \rou
        \\&= \begin{cases}
            \per/2 & \text{for }k=1 \\
            \per/2 & \text{for }k=-1\\
            0 & \text{otherwise}
        \end{cases}
    }
}
Inserting everything we found in this section into \eqref{eq:eigvals18}, we get
\eq{
    \lang x(q(t)) \rang &= \sum_{k=1,-1} \frac 1 2 \exp\lou - i F_0 k \frac{2\pi}{\per} t- \epsilon Q_0 \lou k\frac{2\pi}{\per}\rou^2 t\rou
    \\ &= \cos\lou \frac{2\pi}{\per} F_0 t \rou \exp\lou - \epsilon Q_0 \lou \frac{2\pi}{\per}\rou^2 t\rou
    \,.
}

\section{Why the second solution is irrelevant}\label{sec:gb_sol}
The second solution of \eqref{eq:fpg} is
\eq{
    g(q) = - \frac{F(q)}{Q(q)} + \epsilon \lou - \frac{\lambda}{F(q)} + \frac{Q'(q)}{Q(q)}\rou   + \epsilon^{2} \lou \frac{\lambda^{2} Q(q)}{F^{3}(q)} - \frac{\lambda Q(q) F'(q)}{F^{3}(q)}\rou + \mathcal O\lou \epsilon^3 \rou\,.
}
% Or, to only keep the relevant information:
% \eq{
%     g(q) = - \frac{F(q)}{Q(q)} + \epsilon \lou - \frac{\lambda}{F(q)}\rou
%      + \mathcal O\lou\epsilon^1 \lambda^0\rou
%      + \mathcal O\lou\epsilon^2 \lambda^1\rou
%      + \mathcal O\lou\epsilon^2 \lambda^2\rou
%      + \mathcal O\lou\epsilon^3\rou
% }
Similar to the calculation in section \ref{sec:findeigvals}, the periodic boundary conditions become
\eq{
    0 &= \epsilon 2\pi ki + \int_0^\per \frac{F(q)}{Q(q)} \diff{q} + \epsilon\lambda \int_0^\per \frac{1}{F(q)} \diff{q} + \mathcal O\lou\epsilon^1 \lambda^0\rou
    + \mathcal O\lou\epsilon^2 \lambda^1\rou
    + \mathcal O\lou\epsilon^2 \lambda^2\rou
    %+ \mathcal O\lou\epsilon^3\rou\,,
}
with $k\in\mathbb Z$. The solution can be written in the form
% \eq{
%     0 &= -\epsilon c_1 ki + c_2 + \epsilon\lambda
% }
\eq{
    \lambda &= c_1 ki -\frac{c_2}{\epsilon}\,,
}
with $c_1,c_2\in\mathbb R$ and $c_2 > 0$.
If $\epsilon\ll 1$, the negative real part is large and this is not the first nontrivial eigenvalue.


\section{On the diagonalizability of $\TRM$}\label{sec:jordan}
Back in section \ref{sec:eigvals}, we said that the solution to \eqref{eq:fokker} is \eqref{eq:pxt_eigval}. But this is only true if $\TRM$, the discretized form of $\FPO$ is diagonalizable, i.e. no jordan blocks arise. Otherwise, $p(x,t)$ includes (multiple) additional summands of the form \cite{The_correlation_time_of_mesoscopic_chemical_clocks}
\eq{
    c t^{i} e^{\lambda t} v(x)\,,
}
with $i$ being integer. If there are other eigenvalues with a higher non-zero real part than this $\lambda$, we can ignore such a term, because the term above will fall faster than the other one (because an exponential function falls faster than a polynomial one rises), and we are only interested in the long-time behavior anyway. If this $\lambda$ has the highest non-zero real part, some of our calculations might be incorrect. Further research is required to understand when this will occur.

\section{Discussion of $F(q)>0$ and $Q(q)>0$}\label{sec:F>0Q>0}
In chapter \ref{sec:integral} and \ref{sec:bound}, we assumed that $F(q)>0$ and $Q(q)>0$ holds true for all $q$. In this section, we discuss these assumptions.

$Q(q)\geq 0$, will always hold true, because of its definition in \eqref{eq:Q=C}.
If $Q(q)$ vanishes for all $q$, there is no diffusion, resulting in the system undergoing undamped oscillations. In this case, \eqref{eq:R_myint} correctly predicts $\gamma=0$ i.e. no damping. If $Q(q)$ vanishes for some $q$, the calculations still work the same. \eqref{eq:R_myint} predicts a finite $\gamma$. \eqref{eq:fcont} goes to infinity, but this is not a problem, since it is an upper bound.

If $F(q)<0$ for all $q$, we can substitute $q\rightarrow -q$ to make $F$ flip its sign, resulting in $F(q)>0$ for all $q$. If $F(q)<0$ for some $q$ and $F(q)>0$ for other $q$, there are no oscillations for $\epsilon\rightarrow 0$,
%because the diffusion term in the Langevin equation is the only way for the particle to move in another direction than $F$ acts. However, if $\epsilon$ is finite, then oscillations can occur, but
and \eqref{eq:R_myint} will be wrong. The same argument applies if there is a section with $F(q)=0$. In this case, \eqref{eq:R_myint} correctly predicts $\omega=0$ corresponding to an infinite period, i.e. no oscillations.
