#!/bin/python
from small import *


def example_trajectories():
    fig, ax = plt.subplots()
    ax.add_patch(plt.Circle((0, 0), 1, fill=False))

    for j in range(5):
        N = 1000
        dt = np.pi / N
        xs = np.zeros(N)
        ys = np.zeros(N)
        xs[0] = 1
        for i in range(N - 1):
            F = steward((xs[i], ys[i]), 1, 1, 1, 1)
            xs[i + 1] = (xs[i] + F[0] * dt) + 0.1 * np.random.normal(
                loc=0.0, scale=np.sqrt(dt)
            )
            ys[i + 1] = (ys[i] + F[1] * dt) + 0.1 * np.random.normal(
                loc=0.0, scale=np.sqrt(dt)
            )
        plt.plot(xs, ys)

    pltshow()


def example_trajectory():
    fig, ax = plt.subplots()
    ax.add_patch(plt.Circle((0, 0), 1, fill=False))
    ax.set(title="", xlabel="x", ylabel="y")

    N = 1000
    dt = 5 * np.pi / N
    xs = np.zeros(N)
    ys = np.zeros(N)
    xs[0] = 1
    for i in range(N - 1):
        F = steward((xs[i], ys[i]), 1, 1, 1, 1)
        xs[i + 1] = (xs[i] + F[0] * dt) + 0.1 * np.random.normal(
            loc=0.0, scale=np.sqrt(dt)
        )
        ys[i + 1] = (ys[i] + F[1] * dt) + 0.1 * np.random.normal(
            loc=0.0, scale=np.sqrt(dt)
        )
    plt.plot(xs, ys)

    pltshow()

def example_1D():
    fig, ax = plt.subplots()

    N = 1000
    dt = 20 * np.pi / N

    soa = np.zeros(N)
    for j in range(5):
        xs = np.zeros(N)
        xs[0] = 0
        for i in range(N - 1):
            F = 1
            xs[i + 1] = (xs[i] + F * dt) + 0.1 * np.random.normal(
                loc=0.0, scale=np.sqrt(dt)
            )
            if xs[i + 1] >= 2 * np.pi:
                xs[i + 1] -= 2 * np.pi
            elif xs[i + 1] < 0:
                xs[i + 1] += 2 * np.pi
        soa += xs

        plt.plot(np.arange(0, N) * dt, xs)

    plt.plot(np.arange(0, N) * dt, soa / 5)

    pltshow()

def streamplot_experiments():
    deltamu = 4.0  # 3.8

    xl_min, xl_max, yl_min, yl_max, lcq = limit_cycle_xy_size(deltamu)

    x_min = xl_min - (xl_max - xl_min) * 0.4
    x_max = xl_max + (xl_max - xl_min) * 0.4
    y_min = yl_min - (yl_max - yl_min) * 0.4
    y_max = yl_max + (yl_max - yl_min) * 0.4
    # x_min = -2
    # x_max = 2
    # y_min = -2
    # y_max = 2
    Y, X = np.mgrid[y_min:y_max:100j, x_min:x_max:100j]

    U, V = brüssel_F([X, Y], deltamu)
    # U, V = steward((X, Y), 1, 1, 1, 1)

    # print(find_point_on_limit_cycle(deltamu))
    # U = ar[0]
    # V = ar[1]

    # plt.streamplot(X, Y, U, V, density=[0.5, 1])
    # pltshow()

    # U = -1 - X**2 + Y
    # V = 1 + X - Y**2
    # speed = np.sqrt(U**2 + V**2)

    # w = 3
    # Y, X = np.mgrid[-w:w:100j, -w:w:100j]
    # U = -1 - X**2 + Y
    # V = 1 + X - Y**2
    speed = np.sqrt(U ** 2 + V ** 2)

    fig = plt.figure(figsize=(7, 9))
    gs = gridspec.GridSpec(nrows=2, ncols=2, height_ratios=[1, 1])

    #  Varying density along a streamline
    ax0 = fig.add_subplot(gs[0, 0])
    ax0.streamplot(X, Y, U, V, density=[0.5, 1])
    ax0.set_title("Varying Density")

    # Varying color along a streamline
    ax1 = fig.add_subplot(gs[0, 1])
    strm = ax1.streamplot(X, Y, U, V, color=speed, linewidth=2, cmap="autumn")
    # fig.colorbar(strm.lines)
    ax1.set_title("Varying Color")

    #  Varying line width along a streamline
    ax2 = fig.add_subplot(gs[1, 0])
    lw = 5 * speed / speed.max()
    ax2.streamplot(X, Y, U, V, density=0.6, color="k", linewidth=lw)
    ax2.set_title("Varying Line Width")

    # Controlling the starting points of the streamlines
    # seed_points = np.array([[0.7639918, 0.31366882, 0.7], [1.8748439, 1.60592843, 1.0]])
    # seed_points = np.array([[0.31366882], [1.60592843]])
    seed_points = np.array([[0.1], [1.1]])
    # seed_points = np.array([[0, 0, 0], [0.995, -0.8, -1.3]])

    ax3 = fig.add_subplot(gs[1, 1])
    # strm = ax3.streamplot(
    #     X, Y, U, V, color=U, linewidth=2, cmap="autumn", start_points=seed_points.T
    # )
    # fig.colorbar(strm.lines)
    ax3.set_title("Controlling Starting Points")

    # Displaying the starting points with blue symbols.
    # ax3.plot(seed_points[0], seed_points[1], "bo")
    ax3.plot(lcq[0], lcq[1])
    ax3.set(xlim=(x_min, x_max), ylim=(y_min, y_max))

    # # Create a mask
    # mask = np.zeros(U.shape, dtype=bool)
    # mask[40:60, 40:60] = True
    # U[:20, :20] = np.nan
    # U = np.ma.array(U, mask=mask)

    # ax4 = fig.add_subplot(gs[2:, :])
    # ax4.streamplot(X, Y, U, V, color="r")
    # ax4.set_title("Streamplot with Masking")

    # ax4.imshow(~mask, extent=(-w, w, -w, w), alpha=0.5, cmap="gray", aspect="auto")
    # ax4.set_aspect("equal")

    plt.tight_layout()
    pltshow()
