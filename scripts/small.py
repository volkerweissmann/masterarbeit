#!/bin/python

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import scipy.integrate
from scipy.integrate import RK45
from matplotlib import rc
import json
import os
import sys

rc("text", usetex=True)

OUTDIR = (os.path.dirname(sys.argv[0]) or ".") + "/../plots/"

INTERACTIVE = True


def pltfinal(name):
    plt.tight_layout()
    plt.savefig(OUTDIR + name + ".pdf")
    if INTERACTIVE:
        plt.show()
    plt.clf()


def pltshow():
    if INTERACTIVE:
        plt.show()
    plt.clf()


def add_arrow(line, ind, direction="right", size=15, color=None):
    # Sollte man die pfeile ein klein wenig drehen, wenn sie in einer Kurve sind?
    # from https://stackoverflow.com/questions/34017866/arrow-on-a-line-plot-with-matplotlib
    # see also: https://gist.github.com/coroa/cdcdb1ec1c73d4f6588501e2f7f46c45
    """
    add an arrow to a line.

    line:       line[0] should be an Line2D object
    ind:   position of the arrow
    direction:  'left' or 'right'
    size:       size of the arrow in fontsize points
    color:      if None, line color is taken.
    """
    line = line[0]
    if color is None:
        color = line.get_color()

    xdata = line.get_xdata()
    ydata = line.get_ydata()

    if direction == "right":
        end_ind = ind + 1
    else:
        end_ind = ind - 1

    line.axes.annotate(
        "",
        xytext=(xdata[ind], ydata[ind]),
        xy=(xdata[end_ind], ydata[end_ind]),
        arrowprops=dict(arrowstyle="->", color=color),
        size=size,
    )


def steward(q, a, b, c, d):
    # see function steward_landau_F(p, q)

    x, y = q
    r = np.sqrt(x ** 2 + y ** 2)
    theta = np.arctan2(y, x)

    # let drdt = mu * r - a * r.powi(3);
    # let dthetadt = omega + b * r.powi(2);

    # F_r = c r-a r^3
    # ---
    # F_theta = d + b r^2 tex[\,.]

    drdt = a * r - b * r ** 3
    dthetadt = c + d * r ** 2

    dxdr = np.cos(theta)
    dxdtheta = -np.sin(theta) * r
    dydr = np.sin(theta)
    dydtheta = np.cos(theta) * r

    dxdt = drdt * dxdr + dthetadt * dxdtheta
    dydt = drdt * dydr + dthetadt * dydtheta

    return [dxdt, dydt]


def brüssel_F(q, deltamu):
    A = 1.0
    B = 3.0
    k1p = 0.1
    k1m = 1.0
    k2p = 0.1
    k2m = np.exp(-deltamu) * k1m * k2p * B / (k1p * A)
    k3p = 1.0
    k3m = 1.0

    x, y = q

    return [
        k1p * A - k1m * x + k3p * x ** 2 * y - k3m * x ** 3,
        k2p * B - k2m * y - k3p * x ** 2 * y + k3m * x ** 3,
    ]


def jacobian_brüssel_F(q, deltamu):
    A = 1.0
    B = 3.0
    k1p = 0.1
    k1m = 1.0
    k2p = 0.1
    k2m = np.exp(-deltamu) * k1m * k2p * B / (k1p * A)
    k3p = 1.0
    k3m = 1.0

    x, y = q

    dFxdx = -k1m + 2 * k3p * x * y - 3 * k3m * x ** 2
    dFxdy = k3p * x ** 2
    dFydx = -2 * k3p * x * y + 3 * k3m * x ** 2
    dFydy = -k2m - k3p * x ** 2

    # print(np.dot(np.array([[0, 0], [1, 0]]), np.array([1, 0])))  # [0 1]
    return np.array([[dFxdx, dFxdy], [dFydx, dFydy]])


def hopf():
    pars_a = {"fix": -1, "outer": +1, "inner": +1}
    name = {"fix": "neg", "outer": "pos", "inner": "pos"}
    b = 1

    fig, ax = plt.subplots(figsize=(3.0, 2.2))
    ax.set(title="", xlabel="r", ylabel="$F_r = \mathrm{d}r/ \mathrm{d}t$")
    ax.spines.left.set_position("zero")
    ax.spines.right.set_color("none")
    ax.spines.bottom.set_position("zero")
    ax.spines.top.set_color("none")
    ax.xaxis.set_ticks_position("bottom")
    ax.yaxis.set_ticks_position("left")

    for run in ["fix", "inner"]:
        rs = np.linspace(0, 1.2, 1000)
        if pars_a[run] > 0:
            plt.plot(
                rs,
                pars_a[run] * rs - b * rs ** 3,
                label="$a=+" + str(pars_a[run]) + "$, $b=1$",
            )
        else:
            plt.plot(
                rs,
                pars_a[run] * rs - b * rs ** 3,
                label="$a=" + str(pars_a[run]) + "$, $b=1$",
            )
    plt.legend(bbox_to_anchor=(0.1, 0.1), loc="lower left")
    pltfinal("hopf_1D")

    def deter(run):
        sol = scipy.integrate.solve_ivp(
            lambda t, q: steward(q, pars_a[run], 1, 5, 5),
            (0, 5),
            {"fix": [2, 0], "outer": [2, 0], "inner": [0.1, 0]}[run],
            dense_output=False,
            max_step=1e-3,
        )
        line = plt.plot(sol.y[0], sol.y[1])
        if run == "fix":
            ar = np.linspace(3, 30, 10) ** 2
        elif run == "outer":
            ar = np.linspace(3, 22, 10) ** 2
        else:
            ar = (np.linspace(6, 22, 5) ** 0.5) * 500
        for p in ar:
            add_arrow(line, round(p), size=12)

    fig, ax = plt.subplots(figsize=(3.0, 2.0))
    ax.set(title="", xlabel="x", ylabel="y")
    deter("fix")
    plt.axis("equal")
    pltfinal("hopf_2D_" + name["fix"])

    fig, ax = plt.subplots(figsize=(3.0, 2.0))
    ax.set(title="", xlabel="x", ylabel="y")
    deter("outer")
    deter("inner")
    plt.axis("equal")
    pltfinal("hopf_2D_" + name["outer"])


def example_grouping():
    num = 20
    N = 10001
    dt = 0.01
    xs = np.zeros((num, N))
    ys = np.zeros((num, N))
    for j in range(num):
        xs[j][0] = 1
        for i in range(N - 1):
            F = steward((xs[j][i], ys[j][i]), 1, 1, 1, 1)
            xs[j][i + 1] = (xs[j][i] + F[0] * dt) + 0.1 * np.random.normal(
                loc=0.0, scale=np.sqrt(dt)
            )
            ys[j][i + 1] = (ys[j][i] + F[1] * dt) + 0.1 * np.random.normal(
                loc=0.0, scale=np.sqrt(dt)
            )

    fig = plt.figure(figsize=(6.0, 4.1))
    gs = fig.add_gridspec(2, 3, hspace=0, wspace=0)
    axs = gs.subplots(sharex="col", sharey="row")
    for (i, r) in enumerate(np.linspace(0, (N - 1) ** (1 / 3), 6) ** 3):
        r = round(r)
        if i % 3 == 0 and i // 3 == 1:
            axs[i // 3][i % 3].set(title="", xlabel="x", ylabel="y")
        elif i % 3 == 0:
            axs[i // 3][i % 3].set(title="", xlabel="", ylabel="y")
        elif i // 3 == 1:
            axs[i // 3][i % 3].set(title="", xlabel="x", ylabel="")
        axs[i // 3][i % 3].add_patch(plt.Circle((0, 0), 1, fill=False))
        axs[i // 3][i % 3].plot(xs[:, r], ys[:, r], "x", color="#ff7f0e")
        # if r -1 == 0
        axs[i // 3][i % 3].text(
            0,
            0,
            # "$t = " + str(round(dt * r, 3)) + "$",
            # "$t = " + str(dt * r) + "$",
            "$t = " + str(round(dt * r / np.pi, 2)) + "\\cdot T$",
            verticalalignment="center",
            horizontalalignment="center",
        )
    pltfinal("example_grouping")


def example_1D_ring():
    num = 20
    N = 10001
    dt = 0.01
    phis = np.zeros((num, N))
    for j in range(num):
        phis[j][0] = 0
        for i in range(N - 1):
            F = 2
            phis[j][i + 1] = (phis[j][i] + F * dt) + 0.1 * np.random.normal(
                loc=0.0, scale=np.sqrt(dt)
            )

    # plt.plot(np.cos(phis[7][1:400]))
    # pltshow()

    fig = plt.figure(figsize=(6.0, 4.1))
    gs = fig.add_gridspec(2, 3, hspace=0, wspace=0)
    axs = gs.subplots(
        sharex="col",
        sharey="row",
    )
    for (i, r) in enumerate(np.linspace(0, (N - 1) ** (1 / 3), 6) ** 3):
        r = round(r)
        if i % 3 == 0 and i // 3 == 1:
            axs[i // 3][i % 3].set(title="", xlabel="x", ylabel="y")
        elif i % 3 == 0:
            axs[i // 3][i % 3].set(title="", xlabel="", ylabel="y")
        elif i // 3 == 1:
            axs[i // 3][i % 3].set(title="", xlabel="x", ylabel="")
        axs[i // 3][i % 3].add_patch(plt.Circle((0, 0), 1, fill=False))
        axs[i // 3][i % 3].plot(
            np.cos(phis[:, r]), np.sin(phis[:, r]), "x", color="#ff7f0e"
        )
        # if r -1 == 0
        axs[i // 3][i % 3].text(
            0,
            0,
            # "$t = " + str(round(dt * r, 3)) + "$",
            "$t = " + str(round(dt * r / np.pi, 2)) + "\\cdot T$",
            verticalalignment="center",
            horizontalalignment="center",
        )
    pltfinal("example_1D_ring")


def brüssel_hopf():
    figs = (6.0, 3.6)
    pars_mu = {"fix": 3.8, "outer": 4.4, "inner": 4.4}

    # print(scipy.optimize.root(lambda q: brüssel_F(q, pars_mu["outer"]), (0, 0)))

    def deter(run, q0, targets):

        # solver = RK45(lambda t, q: brüssel_F(q, pars_mu[run]), 0, [2, 0], 10)
        # while solver.status == "running":
        #     solver.step()
        #     print(solver.t, solver.y)

        sol = scipy.integrate.solve_ivp(
            lambda t, q: brüssel_F(q, pars_mu[run]),
            (0, 100),
            q0,
            dense_output=False,
            max_step=1e-2,
        )
        line = plt.plot(sol.y[0], sol.y[1])

        for target in targets:
            sqdist = (sol.y[0] - target[0]) ** 2 + (sol.y[1] - target[1]) ** 2
            ind = np.argmin(sqdist)
            add_arrow(line, ind, size=25)
        # arclen = 0
        # for i in range(1, len(sol.y[0])):
        #     new_arclen = arclen + np.linalg.norm(sol.y[:, i] - sol.y[:, i - 1])
        #     for j in np.arange(1, 10, 0.4):
        #         if arclen < j and new_arclen >= j:
        #             add_arrow(line, i, size=25)
        #     arclen = new_arclen

    def print_mouseclick(event):
        print("[", event.xdata, ",\t", event.ydata, "],")

    fig, ax = plt.subplots(figsize=figs)
    fig.canvas.mpl_connect("button_press_event", print_mouseclick)
    ax.set(title="", xlabel="x", ylabel="y")
    deter(
        "fix",
        [0.1, 3.7],
        [
            [0.16113619281528746, 3.680404382659329],
            [0.26902617912888477, 3.436439959340348],
            [0.40400961492033605, 3.096867856612577],
            [0.5118996012339333, 2.813341634917545],
            [0.618821964351893, 2.5199249636285],
            [0.7533215885455253, 1.9363884375817482],
            [0.6560754573839422, 1.577035435665952],
            [0.4804518473757099, 1.4715373066631492],
            [0.32901881725841864, 1.5078022885078628],
            [0.2056468598146191, 1.6792367481374169],
        ],
    )
    deter(
        "fix",
        [0.1, 3.4],
        [
            [0.15678188843491808, 3.429846326277673],
            [0.265639497944153, 3.2056628021467173],
            [0.39336575976832194, 2.80674800185487],
            [0.5109319780382956, 2.381458669312322],
            [0.501739557679738, 1.7979221432655699],
            [0.3628856291057362, 1.6990176473254424],
            [0.1935515698691485, 1.9133107218623853],
        ],
    )
    deter(
        "fix",
        [0.1, 3.0],
        [
            [0.154362830445824, 3.123242388863278],
            [0.2298374397055602, 3.040821975579838],
            [0.328051194062781, 2.6188294595686274],
            [0.3125692229325787, 2.21991465927678],
            [0.24193272965103074, 2.2363987419334683],
            [0.17371529435857685, 2.5430026793478633],
        ],
    )
    deter(
        "fix",
        [0.1, 2.5],
        [
            [0.12968843895706406, 2.6591330416642296],
        ],
    )
    deter(
        "fix",
        [0.1, 1.5],
        [
            [0.12146364179414408, 1.9363884375817482],
        ],
    )
    pltfinal("hopf_brüssel_neg")

    fig, ax = plt.subplots(figsize=figs)
    fig.canvas.mpl_connect("button_press_event", print_mouseclick)
    ax.set(title="", xlabel="x", ylabel="y")
    deter(
        "outer",
        [0.1, 3.7],
        [
            [0.29118658464052305, 3.8465746228963478],
            [0.6712864294906473, 3.2371969160592573],
            [1.1022329641123891, 2.4547860332066973],
            [1.1172369053564728, 1.6573287872223565],
            [0.8279942602621897, 1.4353949310286014],
            [0.3303635423334087, 1.4542028849433266],
        ],
    )
    deter(
        "inner",
        [0.3, 2.5],
        [
            [0.3420332744121405, 2.609011255307442],
            [0.39204641189242007, 2.146335589005207],
            [0.2153333261287658, 2.522494667299707],
            [0.37537536606566013, 2.845991474632977],
            [0.4720674318608672, 1.6610903780053015],
        ],
    )
    pltfinal("hopf_brüssel_pos")


def find_point_on_limit_cycle(deltamu):
    sol = scipy.integrate.solve_ivp(
        lambda t, q: brüssel_F(q, deltamu),
        (0, 5000),
        [0, 0],
        dense_output=False,
    )
    return sol.y[:, -1]


def limit_cycle_xy_size(deltamu):
    q0 = find_point_on_limit_cycle(deltamu)
    sol = scipy.integrate.solve_ivp(
        lambda t, q: brüssel_F(q, deltamu),
        (0, 50),
        q0,
        dense_output=False,
    )
    return min(sol.y[0]), max(sol.y[0]), min(sol.y[1]), max(sol.y[1]), sol.y


def find_bifurcation_point():
    for mu in np.arange(3.5, 4.5, 0.1):
        sol = scipy.optimize.root(lambda q: brüssel_F(q, mu), (0, 0))
        q0 = sol.x
        J = jacobian_brüssel_F(q0, mu)
        det = np.linalg.det(J)
        tr = np.trace(J)
        delta = tr ** 2 - 4 * det
        print(f"{mu:.2} {det:10.3f} {tr:10.3f} {delta:10.3f}")

    def get_tr(mu):
        sol = scipy.optimize.root(lambda q: brüssel_F(q, mu), (0, 0), tol=1e-12)
        if not sol.success:
            print(sol)
        assert sol.success
        q0 = sol.x
        J = jacobian_brüssel_F(q0, mu)
        tr = np.trace(J)
        return tr

    sol = scipy.optimize.bisect(lambda mu: get_tr(mu), 3.8, 4.4, xtol=1e-20)
    print(sol)


def example_cos1D():
    fig, ax = plt.subplots(figsize=[6.0, 2.5])
    ax.set(title="", xlabel="t", ylabel="x")

    F = 2
    eC = 0.3

    N = 1000
    dt = 5 * np.pi / N
    qs = np.zeros(N)
    qs[0] = 0
    for i in range(N - 1):
        qs[i + 1] = qs[i] + F * dt + eC * np.random.normal(loc=0.0, scale=np.sqrt(dt))

    plt.plot(np.arange(0, N) * dt, np.cos(qs))

    for i in range(1, 5):
        plt.axvline(np.pi / F * 2 * i, color="orange")

    for i in range(0, 5):
        plt.axvline(np.pi / F * (2 * i + 1), color="red")

    pltfinal("example_cos1D")


def cosexp():
    fig, ax = plt.subplots(figsize=[6.0, 2.5])
    ax.set(title="", xlabel="t", ylabel="$\\langle x\\rangle$")

    F = 2
    eC = 0.3

    ts = np.linspace(0, 5 * np.pi, 1000)
    plt.plot(ts, np.cos(ts * F) * np.exp(-ts * eC ** 2 / 2))

    pltfinal("cosexp")


def plt_asin():
    js = json.load(open(os.environ["HOME"] + "/Sync/data/plots/volker_false_2.json"))
    print(js.keys())

    fig, ax = plt.subplots(figsize=(6.0, 4.8))
    ax.set(title="", xlabel="a", ylabel="$\\epsilon R$")

    # plt.errorbar(
    #     np.array(
    #         list(map(lambda x: np.nan if x is None else x, js["100.0"][0]))),
    #     np.array(
    #         list(map(lambda x: np.nan if x is None else x, js["100.0"][1]))),
    #     yerr=np.array(
    #         list(map(lambda x: np.nan if x is None else x, js["100.0"][2]))))

    plt.plot(js["10000.0"][0], js["10000.0"][1], label="Numerics $\\epsilon=10^{-4}$")
    plt.plot(js["1000.0"][0], js["1000.0"][1], label="Numerics $\\epsilon=10^{-3}$")
    plt.plot(js["100.0"][0], js["100.0"][1], label="Numerics $\\epsilon=10^{-2}$")
    plt.plot(js["R_th/Omega"][0], js["R_th/Omega"][1], label="Theory")
    plt.plot(js["f/Omega"][0], js["f/Omega"][1], label="Upper bound")

    plt.legend()
    pltfinal("asin")


def plt_stuart():
    # /Sync/data/plots/cubic_2.json
    js = json.load(
        open(os.environ["HOME"] + "/Sync/data/plots/mail_10_12_x_x0_post_radial_2.json")
    )
    print(js.keys())

    # for b in ["0.0", "0.1", "0.001", "2.0", "0.1", "0.01", "1.0"]:
    #     theo = np.array(js["theo b=" + b][1], dtype=float)
    #     x = {"0.0": 1, "0.001": 2, "0.01": 3, "0.1": 4, "1.0": 5, "2.0": 6}[b]
    #     xar = np.repeat(x, len(theo))
    #     plt.plot(
    #         xar,
    #         np.array(js["gaspard b=" + b][1], dtype=float) / theo,
    #         "x",
    #         label="Gaspard",
    #     )
    #     plt.plot(
    #         xar,
    #         np.array(js["fit b=" + b][1], dtype=float) / theo,
    #         "x",
    #         label="Numerics",
    #     )

    fig = plt.figure(figsize=(6.0, 5.0))
    rows = 2
    cols = 3
    gs = fig.add_gridspec(rows, cols, hspace=0, wspace=0)
    axs = gs.subplots(sharex="col", sharey="row")

    for i in range(rows):
        for j in range(cols):
            if j == 0:
                axs[i][j].set(title="", xlabel="$c$", ylabel="$\\epsilon R$")
            else:
                axs[i][j].set(title="", xlabel="$c$", ylabel="")

    for (i, b) in enumerate(["0.0", "0.01", "0.1", "1.0", "2.0"]):  # "0.001",
        # theo = np.array(js["theo b=" + b][1], dtype=float)
        r = i // cols
        c = i % cols

        axs[r][c].text(
            0 - float(b),
            2000,
            # "$t = " + str(round(dt * r, 3)) + "$",
            # "$t = " + str(dt * r) + "$",
            "$d = " + b + "$",
            verticalalignment="center",
            horizontalalignment="center",
        )

        axs[r][c].plot(
            js["theo b=" + b][0],
            np.array(js["theo b=" + b][1], dtype=float),
        )
        axs[r][c].plot(
            js["gaspard b=" + b][0],
            np.array(js["gaspard b=" + b][1], dtype=float),
        )
        axs[r][c].plot(
            js["fit b=" + b][0],
            np.array(js["fit b=" + b][1], dtype=float),
            "-.",
        )

    axs[rows - 1][cols - 1].plot(
        [],
        [],
        label="Theory",
    )
    axs[rows - 1][cols - 1].plot(
        [],
        [],
        label="Gaspard",
    )
    axs[rows - 1][cols - 1].plot(
        [],
        [],
        "-.",
        label="Numerics",
    )

    axs[rows - 1][cols - 1].legend()  # bbox_to_anchor=(0.3, 0)
    pltfinal("stuart_numerics")


def plt_brussel():
    js = json.load(open(os.environ["HOME"] + "/Sync/data/plots/brüssel_R.json"))
    print(js.keys())

    fig, ax = plt.subplots(figsize=(6.0, 5.0))
    ax.set(title="", xlabel="$\\Delta\\mu$", ylabel="$\\epsilon R$")
    # ax.set(ylim=(0, 0.55))

    labels = {
        "$\\mathrm{xtx0mxtx0}\\,\\,\\Omega=100.0$": "Numerics $\\epsilon=10^{-2}$",
        "$\\mathrm{xtx0mxtx0}\\,\\,\\Omega=1000.0$": "Numerics $\\epsilon=10^{-3}$",
        "$\\mathrm{xtx0mxtx0}\\,\\,\\Omega=10000.0$": "Numerics $\\epsilon=10^{-4}$",
        "Gaspard 2D": "Gaspard",
        "$\\mathrm{tang xtx0mxtx0}\\,\\,\\Omega=100.0$": "Numerics Tang $\\epsilon=10^{-2}$",
        "$\\mathrm{tang xtx0mxtx0}\\,\\,\\Omega=1000.0$": "Numerics Tang $\\epsilon=10^{-2}$",
        "$\\mathrm{tang xtx0mxtx0}\\,\\,\\Omega=10000.0$": "Numerics Tang $\\epsilon=10^{-4}$",
        "Gaspard Tang": "Gaspard Tang",
        "Tang Integral": "Theory",
        "Border": "Upper Boundary",
    }
    for key in reversed(labels.keys()):
        ax.plot(
            js[key][0][1:],
            js[key][1][1:],
            {
                "Gaspard 2D": "-.",
                "$\\mathrm{xtx0mxtx0}\\,\\,\\Omega=100.0$": "-.",
                "$\\mathrm{xtx0mxtx0}\\,\\,\\Omega=1000.0$": "-.",
                "$\\mathrm{xtx0mxtx0}\\,\\,\\Omega=10000.0$": "-.",
                "Gaspard Tang": "-",
                "Border": "-",
                "Tang Integral": "-",
                "$\\mathrm{tang xtx0mxtx0}\\,\\,\\Omega=100.0$": "-",
                "$\\mathrm{tang xtx0mxtx0}\\,\\,\\Omega=1000.0$": "-",
                "$\\mathrm{tang xtx0mxtx0}\\,\\,\\Omega=10000.0$": "-",
            }[key],
            label=labels[key],
        )

    # ax.legend(bbox_to_anchor=(1.0, 0), loc="lower left")
    ax.legend(bbox_to_anchor=(0.0, 1.0), loc="lower left", ncol=2)
    pltfinal("brussel_numerics")


if __name__ == "__main__":
    if len(sys.argv) >= 2 and sys.argv[1] == "rebuild":
        INTERACTIVE = False

        example_cos1D()
        cosexp()
        hopf()
        example_grouping()
        example_1D_ring()
        brüssel_hopf()
        find_bifurcation_point()
        plt_asin()
        plt_stuart()
        plt_brussel()

# def func(event):
#     print(event.xdata, "\t", event.ydata)

# fig, ax = plt.subplots()
# fig.canvas.mpl_connect("button_press_event", func)
# plt.plot(np.random.rand(10))
# pltshow()
