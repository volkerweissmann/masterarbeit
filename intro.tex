\chapter{Introduction}\label{sec:intro}
%\section{Statistical Mechanics}
Statistical mechanics or statistical physics is the science of physical systems that show random behavior.
%The randomness can either come from fundamental randomness in quantum mechanics or from the chaotic de-facto-random nature of some classical systems.
%Some of the deepest theorems in this field concern information: Entropy can be defined as information \cite{naim2008a}, the thermodynamic uncertainty relation relates entropy production to an upper bound on the precision of certain processes \cite{Horowitz2019, Seifert2015}.
Markovian discrete stochastic processes can be described using the master equation \cite{Seifert_2012}. This is known as the microscopic regime. Performing the continuum limit of a special case of the master equation leads to the Fokker-Planck equation. This equation describes a central class of continuous stochastic systems. These systems can also be described using the equivalent but complementary description of the Langevin equation or the path integral \cite{Seifert_2012}. The relative size of the noise is smaller if the system is larger. Thus, increasing the system size yields the mesoscopic regime, where the noise is considered up to first order corrections. This is called the weak noise limit. Performing the limit of infinite system size yields the fully deterministic macroscopic regime. In each of those four regimes - discrete, continuous, weak-noise, deterministic - certain systems exhibit oscillations.
%Cycles with entropy production associated to them is a necessity for oscillations to occur.

%\section{Chemical Reactions}
An example of stochastic systems are chemical networks. There is intrinsic molecular noise in the number of chemical reactions that occur in a given time \cite{Seifert_2012, Gonze673}. The relative size of this noise gets larger if the system gets smaller \cite{Gonze673}. Small biochemical networks consisting of genes, proteins and metabolites \cite{Novk2008} are therefore paradigmatic systems for statistical mechanics \cite{Seifert_2012, Qian2000} \blue{("Biochemical reactions are often on the level of a few
molecules. Hence a stochastic description is required.")}.
A chemical network exhibits oscillations if the reactions form a cycle that is a delayed negative-feedback loop \cite{Novk2008, Harmer2001}.
In nature, this can happen if a protein represses the transcription of its own gene \cite{Theoretical_study_of_robustness_factors}.
Living systems use such oscillators as clocks to time other processes \cite{basile, Cao2015}. There is a trade-off between time precision and entropy/energy cost\cite{Cao2015}, similar to the thermodynamic uncertainty relation which establishes a relation  between entropy production (i.e. energy efficiency) and current precision \cite{Horowitz2019, Seifert2015}. If there is no entropy production associated with the cycle, the system will not oscillate \cite{basile}.


%\subsection{Circadian Rhythm}
A prime example of biochemical oscillators are the circadian clocks controlling the day-night cycle of organisms \cite{basile}.
Among other things, they are interesting because they are able to maintain  their period against global changes like, for instance, a change in the temperature \cite{Barkai2000, Harmer2001}.
Circadian clocks are found in most eukaryotes \cite{doi:10.1146/annurev.cellbio.17.1.215} and have been studied for
plants \cite{doi:10.1146/annurev.cellbio.17.1.215},
mice \cite{doi:10.1126/science.8171325},
\textit{Neurospora} (mold) \cite{10.1111/j.1574-6976.2011.00288.x},
\textit{Synechococcus} (bacteria) \cite{DiamondE1916} and
\textit{Drosophila} (fruit flies) \cite{Rosato2006}.
The latter has been honored with a noble prize \cite{NobelPrize}.
%The eigenfrequencies of those oscillators are not exactly 24 hours, only approximately 24 hours \cite{doi:10.1126/science.8171325} \cite{Leloup1999} \cite{NobelPrize}, but (sun-)light influences the system, the oscillator is driven with 24 hour periodic signal and therefore oscillates with a 24 hour period \cite{NobelPrize} \cite{doi:10.1146/annurev.cellbio.17.1.215} \cite{Leloup1999} \cite{Gonze2000} \cite{Gonze2002}.
In isolation, i.e. in the absence of sunlight, these system oscillate with a mean period of approximately 24 hours. The variance is non zero and the mean depends on the species.
%Since (sun-)light influences the system, the oscillator is driven with a 24 hour periodic signal and therefore oscillates with a 24 hour period \cite{Gonze673} \blue{Gonze673 Fig.5 c,  \cite{doi:10.1126/science.8171325, doi:10.1146/annurev.cellbio.17.1.215, NobelPrize, Leloup1999, Gonze2000, Gonze2002} }.
There is evidence \cite{Barkai2000} that suggests that circadian clocks are embedded in a single cell. Due to the small spatial extent of a single cell, the thermodynamic noise is in comparison quite large \cite{Cao2015}.


%\subsection{Other oscillators}
Further examples for biochemical oscillators include the cell cycle \cite{Ferrell2011, Pomerening2003, Tsai2008}, cAMP signalling \cite{Martiel1987}, the heart beat \cite{Tsai2008}, development of an embryo \cite{Lewis2003} and synthetic oscillators build to learn more about natural oscillators \cite{Elowitz2000}.
The prime example for a chemical oscillator model is the Brusselator, which has been studied by many authors \cite{basile, Nicolis1978}.
Non-chemical examples of noisy oscillators include certain weather systems \cite{burgers1999nino}, populations of species linked through the food web \cite{nisbet1982modelling}, thickness of continental ice sheets \cite{Payne-1995} and epidemic models \cite{Herber-1981}.


%\section{Analyzing Oscillators}
In the deterministic regime, supercritical Hopf bifurcations are the transitions from systems with a stable fixpoint to ones with a stable limit cycle \cite{hale1991dynamics}. If a limit cycle exists, the system exhibits oscillations. Due to the absence of noise, the period is identical for each repetition. Many authors have proposed deterministic models of biochemical oscillators that show a limit cycle. They numerically calculated the shape of the limit cycle \cite{Goldbeter1995, Leloup1999, Leloup1998}.

The limit cycle continues to exists if we transition from the deterministic regime to the weak noise regime. Due to the noise the period fluctuates and is slightly different for each repetition. Multiple ways to quantify this robustness exist \cite{Theoretical_study_of_robustness_factors}: One could plot the probability distribution of first-return times to see how precise the oscillator measures time, or calculate its variance to quantify it. Another method is to analyze autocorrelation functions \cite{Gaspard2002, Barato2017, basile, burgers1999nino}, as its a measure on how similar two sections of the same signal are. Gaspard \cite{Gaspard2002} found a general, but quite involved method to calculate the robustness.

Other authors \cite{Gonze673, McAdams1997} analyzed discrete models of biochemical oscillators. They used the Gillespie algorithm \cite{Gillespie2007} for stochastic simulations. Recently, Barato and Seifert \cite{Barato2017} conjectured an upper bound on the robustness of a discrete stochastic oscillator, linking the robustness to its cycle affinity.

Our main research goal was to find better ways to calculate or put bounds on the robustness of oscillating Langevin systems.
We begin by a brief review of the Langevin, Fokker-Planck and master equation in chapter \ref{sec:stochasticdyn}.
In chapter \ref{sec:chem}, we show that a chemical network follows Langevin dynamics in the continuum limit.
Next, chapter \ref{sec:1D_robustness} explains intuitively how Langevin systems can show damped oscillations. The concept of robustness is explained.
Chapter \ref{sec:eigvals} connects the robustness of such a system to the first nontrivial eigenvalue of the Fokker-Planck operator. In chapter \ref{sec:integral}, we use a similar ansatz as Gaspard \cite{Gaspard2002}, to calculate this eigenvalue and the connected robustness. Restricting ourself to the case of a one-dimensional Langevin system in the weak noise limit, we find a transparent expression for the robustness \eqref{eq:R_myint} of such a system. This is the main result of our research.
In chapter \ref{sec:bound}, we proof that our result lies below the continuous version of the upper bound conjectured by Barato and Seifert \cite{Barato2017}.


Taking a break from stochastic systems, we explain the concept of supercritical Hopf bifurcations in deterministic systems in chapter \ref{sec:hopf}. Coming back to the Langevin equation in chapter \ref{sec:2Drobust} we find that limit cycles can persist in presence of noise. We present an one-dimensional effective description of two-dimensional Langevin system that neglects phase-coupling. We combine our knowledge about limit cycles and chemical networks in chapter \ref{sec:brussel} about the Brusselator. This is a chemical network that shows a limit cycle. To verify the correctness of previous chapters, we compare theoretical predictions to numerical results in chapter \ref{sec:numerics}. We will make some remarks on programming pitfalls you should look out for when reproducing this work.
