\chapter{Stochastic Dynamics}\label{sec:stochasticdyn}
\section{Langevin equation}
One of the most general forms to describe the time-evolution of a classical, continuous, not explicitly time-dependent system is
\eq{\label{eq:qF}
    \partial_t\bv q(t) &= \bv F(\bv q(t))\,,
}
where $\bv q$ is the state and $\Fnt$ is a vector field. An example would be an overdamped particle suspended in a fluid: If the scales are right, the velocity is approximately proportional to the force acting on it, thus the velocity is not a state variable. We could then use the equation above to describe it, with $\bv q$ as the position of the particle and $\Fnt$ proportional to the force. Another example is a network of chemical reactions, with $q_i(t)$ as the concentration of the $i$-molecule and $\Fnt$ as the rate at which molecules are created or destroyed.

If we would shrink the size of these example systems down to a point where the number of molecules making up the system becomes sufficiently small, we would see deviations from \eqref{eq:qF} in the form of apparently random fluctuations. The nature of these fluctuations is rooted in the motion of molecules: Even if their motion is assumed to follow deterministic classical laws, thus being in principle predictable, their motion is too chaotic and difficult to predict in practice so that they are usually described using the language of probabilities. Thus the name ``statistical mechanics''. For many systems, we can extend \eqref{eq:qF} into the stochastic regime by adding a noise term to arrive at the famous Langevin\footnote{Note that some literature is a bit inconsistent in the naming scheme: There is an overdamped Langevin equation and an underdamped Langevin equation. Some authors \cite{Seifert_2012} and this thesis, call the overdamped Langevin equation ``Langevin equation'', while others \cite{risken1989the} call the underdamped Langevin equation ``Langevin equation''.} equation
\eq{\label{eq:lange_ND}
    \partial_t\bv q(t) &= \bv F(\bv q(t)) + \sqrt{\epsilon} \Cn(\bv q(t)) \bv\zeta(t)\,.
}
Here $\zeta(t)$ is delta-correlated Gaussian white noise with mean zero, i.e.
\eq{
    \lang \bv\zeta(t)\rang &= 0\\
    \lang \zeta_i(t)\zeta_j(t')\rang &= \delta_{ij}\delta(t-t')\quad \forall i,j\,,
}
where we have introduced the ensemble-average notation $\lang ... \rang$. An ensemble is a group of (ideally) infinitely many identical independent systems. This $\bv\zeta$ will be our only source of randomness in this thesis. $\Cnt$ indicates the noise strength and $\epsilon$ is proportional to the inverse system size. Since the noise term scales with $\sqrt{\epsilon}$, the limit $\epsilon\rightarrow 0$ would lead us back to \eqref{eq:qF} i.e. fluctuations only appear in small systems. Most authors merge $\epsilon$ and $\Cnt$ into a single variable, but our definition will prove useful later when we will analyze the weak-noise limit $\epsilon\ll 1$.

\section{Fokker-Planck equation}
While \eqref{eq:lange_ND} accurately describes the systems of our interest and can be used to numerically simulate trajectories, it is difficult to analytically make progress using this equation. The Fokker-Planck equation
\eq{\label{eq:fokker}
    \partial_t p(\bv q,t) &= \FPO p(\bv q,t)
}
describes the same dynamics (\cite{Seifert_2012} and (3.77), (3.79) in \cite{ttinger1996stochastic}) in a complementary way. The Langevin equation describes a single trajectory and includes the random $\bv\zeta$. In contrast, the Fokker-Planck equation is fully deterministic and describes the time-evolution of the probability density $p(\bv q)$ for the particle to be at the point $\bv q$. $\FPO$ is the Fokker-Planck operator
\eq{\label{eq:L_fokker}
    \FPO := \sum_i \lou -\ptdiff{q_i} F_i(\bv q) - F_i(\bv q)\ptdiff{q_i} + \sum_j \epsilon \lou \ptdiff{q_i}\ptdiff{q_j} Q_{ij}(\bv q) + 2\ptdiff{q_i}Q_{ij}(\bv q)\ptdiff{q_j} +  Q_{ij}(\bv q)\ptdiff{q_i}\ptdiff{q_j} \rou\rou\,,
}
with
\eq{\label{eq:Q=C}
    \Qn(\bv q) &= \frac 1 2 \Cn(\bv q) \Cn(\bv q)^T\,.
}

\section{Master equation}
While the Langevin and Fokker-Planck equation are at the core of continuous statistical mechanics, we now focus on discrete dynamics. One of the most general discrete, stochastic, classical, not explicitly time-dependent Markovian dynamics is the famous master equation
\eq{\label{eq:master}
    \partial_t p &= \TRM p\,,
}
where $p_i$ is the probability to be in the discrete state $i$ and $\TRM$ is the transition rate matrix that fulfills
\eq{
    \TRE_{ij} &\geq 0 \quad \forall i\neq j\\
    \sum_i \TRE_{ij} &= 0\,,
}
as negative transition rates are forbidden and probability has to be conserved. $\TRE_{ij}$ is the transition rate from state $j$ to state $i$. The similarity of \eqref{eq:master} to \eqref{eq:fokker} is no coincidence, the Fokker-Planck equation is a special case of the continuous version of the Master equation. In section \ref{sec:lambdaproof} and \ref{sec:discretFP} we will discretize the Fokker-Planck equation so that we can apply theorems about the master equation on Langevin systems.
