\chapter{Upper bound on the robustness}\label{sec:bound}
Barato and Seifert \cite{Barato2017} conjectured an upper bound on the robustness of certain oscillating systems. Instead of continuous Langevin systems, they analyzed discrete Markovian unicycle networks. Such a system can be represented by the reaction scheme
\eq{
    E_1 \xrightleftharpoons[k_2^-]{k_1^+} E_2 \xrightleftharpoons[k_3^-]{k_2^+} E_3 ... E_N \xrightleftharpoons[k_1^-]{k_N^+} E_1\,,
}
where $N$ is the number of states and $k_i^\pm$ are the transition rates. The dynamics is given by the master equation \eqref{eq:master}, with the transition rate matrix \cite{Barato2017}
\eq{\label{eq:Lk}
    \TRE_{ij} &= k_j^+ \delta_{i-1,j} + k_j^- \delta_{i+1,j} - \lou k_j^- + k_j^+ \rou \delta_{i,j}\,.
}
This is a $N\times N$ matrix instead of a continuous linear operator, because there are $N$ discrete states instead of a continuous state variable $q$.
According to \cite{Barato2017} \cite{Seifert_2012}, the affinity is defined to be
\eq{\label{eq:Adef}
    \affinity \equiv \left| \ln \prod_{i=1}^N \frac{k_i^+}{k_{i+1}^-} \right|\,,
}
i.e. the logarithm of the probability to complete the cycle in one direction, divided by the probability to complete the cycle in the other direction. The affinity is the amount of entropy that is produced if the particle completes one cycle \cite{Seifert_2012}.
Barato and Seifert \cite{Barato2017} conjectured that
\eq{\label{eq:barato_border}
    R\leq f(\affinity,N)\equiv \cot\frac\pi N \tanh\frac \affinity{2N}
}
is an upper bound on the robustness. They proved it for $N=3$ and showed good numerical evidence up to $N=8$. Equality in the upper equation holds if $k_i^\pm$ is independent of $i$, as can be proven by calculating the eigenvalues of the transition rate matrix \cite{Barato2017}.
In the next chapter, we perform the continuum limit $N\rightarrow\infty$. We will see how their bound translates to
\eq{\label{eq:fcont}
    R\leq f(\affinity) &= \frac 1{2\pi\epsilon} \int_0^\per \frac{F(q)}{Q(q)}
}
for a one-dimensional Langevin system and proof that the robustness \eqref{eq:R_myint} we found in the last chapter is lower than this bound.

\section{Upper bound on the robustness in the continuum limit}\label{sec:discretFP}
We start with the Fokker-Planck equation
\eq{
    \partial_t p(q,t) = \FPO p(q,t)\,.
}
and replace the operator $\FPO$ with a $N\times N$ matrix and the function $p(q)$ with the $N$-dimensional vector $\bv p$ to get
\eq{
    \partial_t \bv p(t) &= \TRM \bv p(t)\,,
}
which is a master equation.
The $i$'th component of $p$ corresponds to $p(i \per/N)$. We define $\Delta q \equiv \per/N$. In the limit $N\rightarrow \infty$, the discrete system will approach the continuous system.
% To find $\TRM$, we start from the one dimensional version of \eqref{eq:contLND_def}
% \eq{\label{eq:contL1D_def}
%     \FPO(q) &\equiv \lou -F'(q) + \epsilon Q''(q)\rou + \lou - F(q) + 2\epsilon Q'(q)\rou \partial_q + \epsilon Q(q)\partial_q^2\,.
% }
We approximate the derivatives using central differences, i.e.
\eq{
    \partial_q f(q) \limeq \frac{f(q+\Delta q) - f(q-\Delta q)}{2\Delta q} \\
    \partial_q^2 f(q) \limeq \frac{f(q+\Delta q) + f(q-\Delta q) - 2 f(q)}{\Delta q^2}\,,
}
which translates to the following matrix expressions,
\eq{\label{eq:numdiff}
    \partial_q \corresponds
    \frac{1}{\Delta q}\begin{pmatrix}
      ...&...&...   &...&...   &...&...\\
      ...&-1/2& \color{red}0 &+1/2&0&0&...\\
      ...&0&-1/2&\color{red}0&+1/2&0&...\\
      ...&0&  0   &-1/2&\color{red}0&+1/2&...\\
      ...&...&...   &...&...   &...&...\\
    \end{pmatrix}\\
    \partial_q^2 \corresponds \frac{1}{\Delta q^2}\begin{pmatrix}
      ...&...&...   &...&...   &...&...\\
      ...&1&\color{red} -2 &1&0&0&...\\
      ...&0&1&\color{red}-2&1&0&...\\
      ...&0&  0   &1&\color{red}-2&1&...\\
      ...&...&...   &...&...   &...&...\\
    \end{pmatrix}\,.
}
Red numbers are the diagonal and periodic indices are assumed.
\blue{
\eq{
    \FPO(q) &\corresponds \TRM\\
    &= \begin{pmatrix}
        \color{red} -F'(1\Delta q) + \epsilon Q''(1\Delta q) & 0 & ...\\
        0 & \color{red} -F'(2\Delta q) + \epsilon Q''(2\Delta q) & ... \\
        ... & ... & ...
    \end{pmatrix} \\
    &+ \begin{pmatrix}
        \color{red} -F(1\Delta q) + 2\epsilon Q'(1\Delta q) & 0 & ...\\
        0 & \color{red} -F(2\Delta q) + 2\epsilon Q'(2\Delta q) & ... \\
        ... & ... & ...
    \end{pmatrix} \frac{1}{\Delta q}\begin{pmatrix}
        ...&...&...   &...&...   &...&...\\
        ...&-1/2& \color{red}0 &+1/2&0&0&...\\
        ...&0&-1/2&\color{red}0&+1/2&0&...\\
        ...&0&  0   &-1/2&\color{red}0&+1/2&...\\
        ...&...&...   &...&...   &...&...\\
    \end{pmatrix} \\
    &+ \begin{pmatrix}
        \color{red} \epsilon Q(1\Delta q) & 0 & ...\\
        0 & \color{red} \epsilon Q(2\Delta q) & ... \\
        ... & ... & ...
    \end{pmatrix} \frac{1}{\Delta q^2}\begin{pmatrix}
        ...&...&...   &...&...   &...&...\\
        ...&1&\color{red} -2 &1&0&0&...\\
        ...&0&1&\color{red}-2&1&0&...\\
        ...&0&  0   &1&\color{red}-2&1&...\\
        ...&...&...   &...&...   &...&...\\
    \end{pmatrix}
}
}%
Using this discretization and \eqref{eq:contL1D_def}, we find the non-zero matrix elements of $\TRM$ to be given by
\eq{
\TRE_{i,i} &=  -F'(i\Delta q) + \epsilon Q''(i\Delta q) - \frac{2\epsilon Q(i\Delta q)}{\Delta q^2}\\
\TRE_{i-1,i} &=  \frac{-F((i-1)\Delta q)+2\epsilon Q'((i-1)\Delta q)}{2\Delta q} + \frac{\epsilon Q((i-1)\Delta q)}{\Delta q^2}\\
\TRE_{i+1,i} &=  \frac{+F((i+1)\Delta q)-2\epsilon Q'((i+1)\Delta q)}{2\Delta q} + \frac{\epsilon Q((i+1)\Delta q)}{\Delta q^2}\,.
}
%Since the discrete system only behaves like the continuous system in the continuum limit $\Delta q\rightarrow 0$,
Since we perform the continuum limit $\Delta q\rightarrow 0$, the we can add terms of higher $\Delta q$ order to get to
\eq{
    \TRE^{i-1,i} &= \frac{\epsilon Q((i-1)\Delta q)}{\Delta q^2}\exp\lou-\frac{(F((i-1)\Delta q)-2\epsilon Q'((i-1)\Delta q))\Delta q}{2\epsilon Q((i-1)\Delta q)} \rou \\
    \TRE^{i+1,i} &= \frac{\epsilon Q((i+1)\Delta q)}{\Delta q^2}\exp\lou+\frac{(F((i+1)\Delta q) -2\epsilon Q'((i+1)\Delta q))\Delta q}{2\epsilon Q((i+1)\Delta q)} \rou \\
    \TRE^{i,i} &= - \TRE^{i+1,i} - \TRE^{i-1,i}\,,
}
which will proof to be more convenient. By comparing the equations above to \eqref{eq:Lk}, we find
\eq{
    \affinity &= \sum_{i=1}^N \frac{(F(i\Delta q) -2\epsilon Q'(i\Delta q))\Delta q}{\epsilon Q(i\Delta q)}\,.
}
In the continuum limit
\eq{
    \Delta q \equiv \frac{\per}{N} \rightarrow 0\,,
}
this sum becomes the integral
\eq{
    \affinity &= \int_0^\per \frac{F(q)-2\epsilon Q'(q)}{\epsilon Q(q)} \diff{q}\,.
}
Since $Q(q)$ is a periodic function, we can use
\eq{
    0 &= \ln Q(\per) - \ln Q(0) = \int_0^\per \frac{Q'(q)}{Q(q)} \diff{q}\,
}
to find
\eq{
    \affinity &= \int_0^\per \frac{F(q)}{\epsilon Q(q)} \diff{q}\,.
}
For large $N$, the bound \eqref{eq:barato_border} becomes
\eq{
    f(\affinity)&= \frac{\affinity}{2\pi}
    \\&=\frac 1{2\pi\epsilon} \int_0^\per \frac{F(q)}{Q(q)}\,,
}
thus proving \eqref{eq:fcont}.

\section{Proving that our $R$ lies below the bound}
The Cauchy Schwartz inequality states that any two real functions $f(q)$, $g(q)$ that are square-integrable on $[0,\per]$ fullfill
\eq{\label{eq:cauchy}
    \lou \int_0^\per f(q) g(q)\diff{q} \rou^2 \leq \int_0^\per f(q)^2 \diff{q} \int_0^\per g(q)^2 \diff{q}\,.
}
If $F(q)>0$ and $Q(q)>0$ (see section \ref{sec:F>0Q>0}),
\eq{
    f(q) \equiv \sqrt\frac{F(q)}{Q(q)} \\
    g(q) \equiv \sqrt\frac{Q(q)}{F^3(q)}
}
are two real functions. Inserting into \eqref{eq:cauchy} yields
\eq{
    \lou \int_0^\per\frac 1{F(q)}\diff{q}\rou^2 &= \lou \int_0^\per \sqrt\frac{F(q)}{Q(q)} \sqrt\frac{Q(q)}{F^3(q)} \diff{q} \rou^2 \leq \lou\int_0^\per\frac{F(q)}{Q(q)}\diff{q} \rou \lou \int_0^\per \frac{Q(q)}{F^3(q)}\diff{q} \rou\,,
}
which can be rearranged to the form
\eq{
    \frac{1}{2\pi\epsilon}\lou \int_0^\per\frac 1{F(q)}\diff{q}\rou^2 \lou \int_0^\per \frac{Q(q)}{F^3(q)}\diff{q} \rou^{-1} &\leq \frac{1}{2\pi\epsilon}\int_0^\per\frac{F(q)}{Q(q)}\diff{q} \,.
}
This proofs that our R \eqref{eq:R_myint} always lies below Barato and Seiferts bound \eqref{eq:fcont}. Equality holds if $F$ and $Q$ are constant.
%To conclude, there is proof that \eqref{eq:barato_border} is an upper bound on the robustness for $N=3$, there is good numerical evidence that it holds $N=4,5,6,7,8$ \cite{Barato2017} and we showed a proof for $N\rightarrow\infty$ (continuum limit) with $\epsilon\ll 1$ (weak noise limit). Our findings indicate that \eqref{eq:barato_border} might be an upper bound for all $N$ and all $\epsilon$, i.e. that their conjecture is true. There is still no proof for the general case, but proofs of special cases are an indication that the general case might be true.
