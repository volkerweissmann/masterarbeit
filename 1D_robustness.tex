\chapter{Damped oscillations in Langevin systems}\label{sec:1D_robustness}
In this chapter, we explains intuitively how Langevin systems can show damped oscillations and the concept of robustness.
For simplicity, we initially only look at the one-dimensional case of the Langevin equation \eqref{eq:lange_ND}, but chapter \ref{sec:2Drobust} will look at two-dimensional systems.
Since we are interested in oscillations, we impose periodic boundary conditions on $q$ with period $\per$, i.e.
\eq{
    F(q+\per)&=F(q) \\
    C(q+\per)&=C(q)
}
and only look at observables $x$ that fulfill
\eq{
    x(q+\per) &= x(q)\,.
}
As the simplest example, we look at the observable
\eq{
    x(q)\equiv\cos\lou\frac{2\pi}{\per}q\rou
}
in a Langevin system with constant force and diffusion, $F(q)=F_0$ and $C(q)=C_0$. In the deterministic limit, i.e. $\epsilon \rightarrow 0$, the Langevin equation reduces to
\eq{
    \partial_t q(t) &= F_0\,.
}
Assuming $q=0$ as the initial position of the particle, this implies
\eq{
    x(t) = \cos\lou\frac{2\pi }{\per} F_0 t \rou\,,
}
i.e. $x$ oscillates without damping, every peak has the height $1$ and is exactly $\per/F_0$ after the previous peak. If we now look at a non-zero $\epsilon$, the equation above becomes
\eq{
    x(t) = \cos\lou\frac{2\pi}{\per}\lou F_0 t + \sqrt{\epsilon} C_0\int_0^{t}\diff{t'} \zeta(t') \rou  \rou\,.
}
The distance between each peak is now different and random for each peak, but each peak still has the same height of $1$. One example trajectory is shown in figure \ref{fig:example_cos1D}.

We now no longer look at a single system, but a group of infinitely many identical and independent systems, called an ensemble. \blue{Es gibt bestimmt eine schönere formulierung als we now no longer look} As proven in section \ref{sec:const_cosexp}, we find the ensemble average to be
\eq{\label{eq:const_cosexp}
    \lang x(t)\rang = \cos\lou\frac{2\pi}\per F_0  t\rou \exp\lou - t\lou\frac{2\pi}{\per}\rou^2\epsilon C_0^2/2 \rou \,.
}
As shown in figure \ref{fig:cosexp}, the height of the peaks now falls off as $t$ grows. %The reason for this is that while the peaks of each single system have a height of one, they dephase and loose coherence.
We can understand this by looking at figure \ref{fig:example_1D_ring}. It shows an ensemble of 20 systems at different times. The deterministic component $F_0$ of $\partial_t q(t)$ results in the group moving counterclockwise along the ring. The stochastic component $\sqrt{\epsilon}C_0\zeta(t)$ results in the group spreading out as time increases. This diffusion is the reason that the peaks of the average fall off as time increases, because the systems do not peak at the same time, they loose coherence. If there would be a small force that is periodic in time, this would potentially keep the systems in sync. For circadian clocks in nature, sunlight fulfills this role \cite{Gonze673}. But this thesis focusses exclusively on self-sustained oscillations that are not driven by a time-dependent force.
As shown in the next chapter, in many systems \eqref{eq:const_cosexp} becomes something like
\eq{
    \lang x(t)\rang = x_s + \cos\lou \omega t + \varphi t\rou e^{-t\gamma} + h(t)\,,
}
where $x_s,\varphi,\omega,\gamma\in\mathbb R$ and $h(t)$ is a function that decays faster than $e^{-t\gamma}$. We are interested in
\eq{
    R \equiv |\omega/\gamma| \,,
}
which is called the robustness or quality factor of the oscillation and is the number of oscillations after which the oscillations decay by a factor of $e^{2\pi}\approx 535$, if we only look at the behavior for large $t$.


\begin{figure}
    \includegraphics{plots/example_cos1D.pdf}
    % source: https://tex.stackexchange.com/questions/370490/equation-environment-in-caption/370498
    \captionsetup{singlelinecheck=off}
    \caption[.]{
        Example trajectory of
        \eq{
            x(t)=\cos(q(t))\,,
        }
        where $q$ follows Langevin dynamics with $F=2$, $\sqrt{\epsilon} C=0.3$. The vertical lines mark where the peaks would be in the deterministic limit ($\epsilon \rightarrow 0$). The height of the peaks is $\pm 1$, as in the deterministic limit, but the $t$-position of the peaks does not exactly match those of the deterministic peaks. The deterministic solution is
        \eq{
            x(t)=\cos(F_0 t) =\cos(2 t) \,,
        }
        therefore this plot looks like a cosine function with some noise. The average square-distance between the $t$-position of a peak and the $t$-position of the corresponding deterministic peak increases linear as $t$ increases, even though we cannot properly see this in this figure, because it shows just a single trajectory over just 5 periods.
    }
    \label{fig:example_cos1D}
\end{figure}
\begin{figure}
    \includegraphics{plots/cosexp.pdf}
    \caption{Ensemble average $\lang x(t)\rang=\lang\cos(q(t))\rang$, where $q$ follows Langevin dynamics with $F=2$, $\sqrt{\epsilon} C=0.3$. This is exactly \eqref{eq:const_cosexp} with $\per=2\pi$. The $t$-position of the peaks is the same as in the deterministic limit ($\epsilon\rightarrow 0$), but the height of the peaks falls off exponentially as $t$ increases.}
    \label{fig:cosexp}
\end{figure}
\begin{figure}
    \includegraphics{plots/example_1D_ring.pdf}
    \caption{Time evolution of 20 one-dimensional periodic systems with $F(q)=2$, $\sqrt{\epsilon}C(q)=1$. The group moves counterclockwise (because of $F$) and it spreads out as time increases (because of $C$). Taking the mean of the $x$ coordinate would result in approximately \eqref{eq:const_cosexp}.}
    \label{fig:example_1D_ring}
\end{figure}
